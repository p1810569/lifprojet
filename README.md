<!--
 Copyright 2021 matthieu
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->

# ALLUVION

    git clone git@forge.univ-lyon1.fr:p1810569/qtopencl
    git submodule init
    git submodule update

## Members

>   [**Matthieu JACQUEMET p1810569**<br>](https://forge.univ-lyon1.fr/p1810569)
>   [**Khaled MAHAZZEM p1809947**<br>](https://forge.univ-lyon1.fr/p1809947)
>   [**Charles ENG p1403762**<br>](https://forge.univ-lyon1.fr/p1403762)


## Description

Alluvion is a corss-platform graph based procedural terrain generator.
The goal of this project is to provide an opensource and free alternative 
to commercial products such as WorldMachine, Terragen, Vue...


## Prerequisites

    cmake
    qt5
    opencl
    opengl


## Build

    mkdir build
    cd build
    cmake .. [-DCMAKE_BUILD_TYPE=<config>]
    cmake --build . [--target <target>] [--parallel <proc>]

 - config : (Debug | Release | MinSizeRel | RelWithDebiInfo)
 - target : target to build (alluvion)
 - proc   : how many compilation units to run in parallel (use `nproc` to know how many core are available)

 
 ## Run

    build/alluvion [options]