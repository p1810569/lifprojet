// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QPluginLoader>
#include <DockWidget.h>

#include "utils.h"
#include "editorPlugin.h"
#include "terrainStagePlugin.h"
#include "pluginManager.h"


using namespace std;


void PluginManager::loadPlugins(const QDir& path) {

    // we first load static plugins
    QVector<QStaticPlugin> plugins = QPluginLoader::staticPlugins();

    for (QStaticPlugin& plugin : plugins) {
        QObject* instance = plugin.instance();

        if (instance != Q_NULLPTR) {
            QJsonObject metadata = plugin.metaData();
            QString iid = metadata["IID"].toString();

            registerPlugin(iid, instance);
        }
    }
    // and then dynamic ones
    for (const QString& basename: path.entryList(QDir::Files)) {
        QString fullpath = path.absoluteFilePath(basename);

        if (QLibrary::isLibrary(fullpath))
            loadPlugin(fullpath);
    }  
}


bool PluginManager::loadPlugin(const QString& name) {

    QPluginLoader loader(name);
    QObject* plugin = loader.instance();

    if (plugin != Q_NULLPTR) {
    
        QJsonObject metadata = loader.metaData();
        QString iid = metadata["IID"].toString();

        if (!registerPlugin(iid, plugin))
            loader.unload();
    } else
        qDebug() << loader.errorString();

    return false;
}


bool PluginManager::registerPlugin(const QString& iid, QObject* plugin) {

    Handlers::iterator it = handlers.find(iid);
    
    if (Q_UNLIKELY(it == handlers.end()))
        return false;
    
    return it.value()->registerPlugin(plugin);
}


INITIALIZER(plugin_manager) {
    
    PluginManager* mgr = PluginManager::globalInstance();
    mgr->registerHandler<TerrainStagePlugin>();
    mgr->registerHandler<EditorPlugin>();
}
