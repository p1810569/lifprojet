// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QToolButton>
#include <QStyle>
#include <QDebug>
#include <QMenu>
#include <QPainterPath>
#include <QListWidget>
#include <QListView>
#include <QStyledItemDelegate>

#include "widgets.h"



DoubleSpinBox::DoubleSpinBox(QWidget* parent):
    QDoubleSpinBox(parent)
{

    setLineEdit(new DragLineEdit<QDoubleSpinBox>(*this));    
}


IntSpinBox::IntSpinBox(QWidget* parent):
    QSpinBox(parent)
{

    setLineEdit(new DragLineEdit<QSpinBox>(*this));    
}


Slider::Slider(QWidget* parent):
    SliderEdit(parent),
    is_pressing(false)
{

}

void Slider::mousePressEvent(QMouseEvent* event) {
    SliderEdit::mousePressEvent(event);
    is_pressing = true;
}

void Slider::mouseReleaseEvent(QMouseEvent* event) {
    SliderEdit::mouseReleaseEvent(event);
    is_pressing = false;
}


void Slider::mouseMoveEvent(QMouseEvent* event) {
    
    if (is_pressing)
        SliderEdit::mouseMoveEvent(event);
}



ComboBox::ComboBox(QWidget* parent):
    QComboBox(parent)
{
    setView(new QListView);
    setModel(view()->model());
	setItemDelegate(new QStyledItemDelegate(this));

    setStyleSheet(Application::globalInstance()->styleSheet());
}



void ComboBox::showPopup() {

    QComboBox::showPopup();

    QWidget* widget = view()->parentWidget();
    Application* app = Application::globalInstance();

    QPoint pos = mapToGlobal(QPoint(0, height()));
    QPoint win_pos = app->window.pos();
    QSize win_size = app->window.size();

    widget->setFixedWidth(width());
    const int margin = 5;

    int max_x = win_pos.x() + win_size.width();
    int max_y = win_pos.y() + win_size.height();

    int w = widget->width() + margin;
    int h = widget->height() + margin;


    if (pos.x() + margin < win_pos.x())
        pos.setX(win_pos.x() + margin);

    if (pos.y() + margin < win_pos.y())
        pos.setY(win_pos.y() + margin);

    if (pos.x() + w > max_x)
        pos.setX(max_x - w);

    if (pos.y() + h > max_y)
        pos.setY(max_y - h);

    widget->move(pos);
}



SpinBoxVectorBase::SpinBoxVectorBase(uint8_t n, QWidget* parent):
    QWidget(parent)
{
    QVBoxLayout* layout = new QVBoxLayout;

    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(0);
    layout->setMargin(0);

    setLayout(layout);

    for (uint8_t i=0; i<n; ++i) {

        DoubleSpinBox* spinbox = new DoubleSpinBox(this);

        layout->addWidget(spinbox);
        spinboxes.append(spinbox);
    }

    spinboxes[0]->setProperty("isFirst", true);
    spinboxes.back()->setProperty("isLast", true);
}




ComboBoxWidget::ComboBoxWidget(const QString& name, QListWidgetItem* _item):
    text(name),
    item(_item)
{
    initUi();
}


void ComboBoxWidget::initUi() {

    setLayout(new QHBoxLayout(this));
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);

    QToolButton* del_ptn = new QToolButton(this);
    del_ptn->setAutoRaise(true);
    del_ptn->setToolTip(tr("Delete layout"));

    connect(del_ptn, SIGNAL(clicked(bool)),
            this, SLOT(removeRequested));

    layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                                            QSizePolicy::Minimum));
    layout()->addWidget(del_ptn);
}


QString ComboBoxWidget::name() const {
    return text;
}


void ComboBoxWidget::removeRequested() {
    Q_EMIT removeItem(item);
}



ListComboBox::ListComboBox(QWidget* parent):
    QComboBox(parent),
    list(new QListWidget(this))
{
    
    setModel(list->model());
    setView(list);
}


void ListComboBox::addItem(const QString& entry) {

    QListWidgetItem* item = new QListWidgetItem(list);
    ComboBoxWidget* item_widget = new ComboBoxWidget(entry, item);

    connect(item_widget, SIGNAL(removeItem(QListWidgetItem*)),
            this, SLOT(removeItem(QListWidgetItem*)));

    item->setSizeHint(item_widget->sizeHint());
    list->addItem(item);
    list->setItemWidget(item, item_widget);
}


void ListComboBox::removeItem(QListWidgetItem* item) {

    int row = list->row(item);
    delete list->takeItem(row);
}

QStringList ListComboBox::itemNames() const {
    
    QStringList names;

    int num = list->count();

    for (int i=0; i<num; ++i) {
        QWidget* widget = list->itemWidget(list->item(i));
        ComboBoxWidget* item = static_cast<ComboBoxWidget*>(widget);
        names.append(item->name());
    }
    return names;
}
