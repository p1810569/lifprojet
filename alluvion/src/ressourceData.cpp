// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QJsonArray>

#include "color.h"
#include "ressourceData.h"

using DataPtr = TerrainStage::DataPtr;


DataPtr RessourceFactory::createRessource(const QString& id) {
    
    static RessourceFactory* instance = globalInstance();

    Entries::const_iterator it = instance->entries.find(id);

    if (it != instance->entries.end()) {
        const Entry entry = it.value();
        return entry.creator();
    }
    return Q_NULLPTR;
}


DataPtr RessourceFactory::getDefault(const QString& id) {
    
    static RessourceFactory* instance = globalInstance();

    Entries::const_iterator it = instance->entries.find(id);

    if (it != instance->entries.end()) {
        const Entry entry = it.value();
        return entry.default_data;
    }
    return Q_NULLPTR;
}


void RessourceFactory::setupDefaults(Terrain& terrain) {

    for (Entry& entry : globalInstance()->entries.values())
        entry.default_data->setup(terrain);
}



template<>
void Ressource<QCLImage2D>::setup(Terrain& terrain) {

    value = terrain.context.createImage2DDevice(default_format,
        terrain.size(), QCLMemoryObject::ReadOnly);
}

template<>
void Ressource<QCLImage3D>::setup(Terrain& terrain) {
    
    QSize size = terrain.size();

    value = terrain.context.createImage3DDevice(default_format,
        size.width(), size.height(), 2, QCLMemoryObject::ReadOnly);
}

template<>
void Ressource<QCLBuffer>::setup(Terrain& terrain) {
    value = terrain.context.createBufferDevice(1, QCLMemoryObject::ReadOnly);
}


template<>
QJsonValue Ressource<Color>::serialize() const {
    return QJsonArray({value[0], value[1], value[2], value[3]});
}


template<>
void Ressource<Color>::deserialize(const QJsonValue& _value) {
    QJsonArray array = _value.toArray();

    value = Color(  array[0].toInt(), array[1].toInt(),
                    array[2].toInt(), array[3].toInt());
}


template<>
QJsonValue Ressource<QCLImage2D>::serialize() const { return QJsonValue(); }
template<>
void Ressource<QCLImage2D>::deserialize(const QJsonValue& _value) {}

template<>
QJsonValue Ressource<QCLImage3D>::serialize() const { return QJsonValue(); }
template<>
void Ressource<QCLImage3D>::deserialize(const QJsonValue& _value) {}

template<>
QJsonValue Ressource<QCLSampler>::serialize() const { return QJsonValue(); }
template<>
void Ressource<QCLSampler>::deserialize(const QJsonValue& _value) {}

template<>
QJsonValue Ressource<Null>::serialize() const {
    qWarning() << "Cannot serialize null type";
    return QJsonValue(); 
}

template<>
void Ressource<Null>::deserialize(const QJsonValue& _value) {
    qWarning() << "Cannot deserialize null type";
}


template<>
void Ressource<Null>::set(QCLKernel& kernel, int index) {
    qWarning() << "Cannot set null ressource as kernel input";
}




REGISTER_RESSOURCE_TYPE(QCLImage2D)
REGISTER_RESSOURCE_TYPE(QCLImage3D)
REGISTER_RESSOURCE_TYPE(QCLSampler)
REGISTER_RESSOURCE_TYPE(QCLBuffer)
REGISTER_RESSOURCE_TYPE(cl_int)
REGISTER_RESSOURCE_TYPE(cl_uint)
REGISTER_RESSOURCE_TYPE(cl_long)
REGISTER_RESSOURCE_TYPE(cl_ulong)
REGISTER_RESSOURCE_TYPE(QVector2D)
REGISTER_RESSOURCE_TYPE(QVector3D)
REGISTER_RESSOURCE_TYPE(QVector4D)
REGISTER_RESSOURCE_TYPE(QPoint)
REGISTER_RESSOURCE_TYPE(QPointF)
REGISTER_RESSOURCE_TYPE(Color)
REGISTER_RESSOURCE_TYPE(Null)
REGISTER_RESSOURCE_TYPE(float)



const QCLImageFormat default_format(
    QCLImageFormat::Order_R,
    QCLImageFormat::Type_Half_Float
);

