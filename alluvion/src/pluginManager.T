// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


template<class T> bool PluginManager::registerHandler() {

    QString iid = qobject_interface_iid<T*>();

    if (Q_UNLIKELY(handlers.contains(iid)))
        return false;

    handlers[iid].reset(new PluginHandler<T>);
    return true;
}


template<class T> PluginHandler<T>* PluginManager::getHandler() const {

    QString iid = qobject_interface_iid<T*>();
    Handlers::const_iterator it = handlers.find(iid);

    if (Q_LIKELY(it != handlers.end()))
        return static_cast<PluginHandler<T>*>(it.value().data());
    
    return Q_NULLPTR;
}


template<class T> bool PluginManager::unregisterHandler() {
    
    QString iid = qobject_interface_iid<T*>();
    return handlers.remove(iid) > 0;
}


template<class T>
QList<QSharedPointer<T>> PluginManager::getPlugins() const {

    PluginHandler<T>* handler = getHandler<T>();

    if (handler != Q_NULLPTR)
        return handler->values();

    return QList<QSharedPointer<T>>();
}


template<class T>
T* PluginManager::getPlugin(const QString& id) const{

    PluginHandler<T>* handler = getHandler<T>();

    if (handler != Q_NULLPTR) {
        typename PluginHandler<T>::iterator it = handler->find(id);

        if (it != handler->end())
            return it.value().get();
    }
    return Q_NULLPTR;
}



template<class T>
bool PluginHandler<T>::registerPlugin(QObject* plugin) {

    QSharedPointer<T> ptr(qobject_cast<T*>(plugin));

    if (Q_UNLIKELY(ptr == Q_NULLPTR))
        return false;

    const QMetaObject* info = plugin->metaObject();
    Container::insert(info->className(), qMove(ptr));

    return true;
}