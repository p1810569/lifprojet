// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SELECTLAYOUTWIDGET_H__
#define __SELECTLAYOUTWIDGET_H__

#include <DockManager.h>
#include <QPushButton>
#include <QAction>

class QActionGroup;
class Layout;


class SelectLayoutWidget: public QPushButton {

	Q_OBJECT

public:
	SelectLayoutWidget(ads::CDockManager& mgr);
	virtual ~SelectLayoutWidget() = default;

	QByteArray serialize() const;
	void deserialize(const QByteArray& buffer);

	QMenu* menu;

private Q_SLOTS:
	void addLayout();

private:
	QString getUnique(const QString& name) const;
	Layout* current() const;
	int enableFirst(bool enable);

	ads::CDockManager& mgr;
	QMenu* remove_menu;
	QAction* save_action;
	QAction* separator;

	mutable QActionGroup* layouts;

	friend class Layout;
};




class Layout: public QAction {

    Q_OBJECT
public:
    Layout(	const QString& name, SelectLayoutWidget& widget);
    virtual ~Layout();

    QByteArray data() const;
    void setEnabled(bool enabled);
	void checkNext();

private:
	SelectLayoutWidget& widget;
    QAction* remove;
};


#endif // __SELECTLAYOUTWIDGET_H__