#include "mainWindow.h"
#include "application.h"
#include "pluginManager.h"



int main(int argc, char** argv) {

	Application app(argc, argv);
	return app.exec();
}
