// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __TERRAIN_H__
#define __TERRAIN_H__

#include <nodes/FlowScene>
#include <QtCore/QSet>
#include <QtCore/QSharedPointer>

#include "mesh.h"
#include "qclcontext.h"

class TerrainStage;
class Application;


class Terrain: public QtNodes::FlowScene {

    Q_OBJECT

public:
    Terrain(Application* app);
    virtual ~Terrain() = default;
    Q_DISABLE_COPY(Terrain)

    enum BufferType {Height, Color, COUNT};

    void setGridSize(const QSize& grid_size);
    void setDeviceType(QCLDevice::DeviceType type);

    QSize size() const;
    Mesh mesh() const;

    typedef std::array<QImage, COUNT> Buffers;

    Buffers buffers;
    QCLContext context;

Q_SIGNALS:
    void contextChanged(QCLContext& context);
    void clearBuffer(Terrain::BufferType type, Qt::GlobalColor color);
    void updateBuffer(Terrain::BufferType type, QCLImage2D& image);

private Q_SLOTS:
    void setupNode(QtNodes::Node& node);
    void destroyNode(QtNodes::Node& node);

protected:
    void setupContext();

    QSize grid_size;

    QCLDevice::DeviceType dev_type;

    QSharedPointer<TerrainStage> node;
};


#endif // __TERRAIN_H__