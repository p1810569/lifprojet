// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __MAINWINDOWPRIVATE_H__
#define __MAINWINDOWPRIVATE_H__

#include <QTimer>
#include <QLabel>
#include <QStatusBar>
#include <QLinkedList>
#include <DockManager.h>

#include "selectLayoutWidget.h"


class MainWindow;
class ComboBox;
class QPushButton;






class StatusEntry: public QLabel {
	
	Q_OBJECT

public:
	typedef std::function<QString(StatusEntry*)> UpdateFunc;

	StatusEntry(const QString& name, UpdateFunc func, int interval);
	virtual ~StatusEntry() = default;

	QAction action;

private:
	UpdateFunc callback;
	QTimer timer;

private Q_SLOTS:
	void update();
};



class StatusBar: public QStatusBar {

	Q_OBJECT

public:
	typedef StatusEntry::UpdateFunc UpdateFunc;

	StatusBar(MainWindow* window=Q_NULLPTR);
	virtual ~StatusBar() = default;

	void addStatusEntry(const QString& name, UpdateFunc func, int interval=1000);

	QMenu menu;

private:
	typedef QVector<StatusEntry*> Entries;
	Entries entries;

public Q_SLOTS:
	void showContextMenu(const QPoint& pos);
};



class MainWindowPrivate: public QObject {

public:
	MainWindowPrivate(MainWindow* win);
	~MainWindowPrivate() = default;

	void createEditors();
	void initStatusBar();
	void setupUI();

	void loadLayouts(const QByteArray& data);
	QByteArray saveLayouts() const;

	MainWindow* q_ptr;
	StatusBar status_bar;
	ads::CDockManager dock_manager;
	SelectLayoutWidget select_layout;
};


#endif // __MAINWINDOWPRIVATE_H__



// class SelectLayoutWidget: public QWidget {

// 	Q_OBJECT

// public:
// 	SelectLayoutWidget(ads::CDockManager& mgr);
// 	virtual ~SelectLayoutWidget() = default;

// 	void setCurrentLayout(int index);

// 	QByteArray serialize() const;
// 	void deserialize(const QByteArray& buffer);

// private Q_SLOTS:
// 	void removeRequested();
// 	void createRequested();
// 	void changeRequested();
// 	void renameRequested();

// private:
// 	void setupActions();
// 	ads::CDockManager& mgr;
// 	int current;

// 	ComboBox* combo;
// 	QPushButton* add_btn;
// 	QPushButton* del_btn;

// 	QString getUnique(const QString& name) const;
// };



