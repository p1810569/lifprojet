// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __EDITORPLUGIN_H__
#define __EDITORPLUGIN_H__

#include <QtCore/QObject>
#include <QtCore/QString>

/**
 * @file        editorPlugin.h
 * @brief       Contient la déclaration de la classe EditorPlugin
 * @details     La classe \c EditorPlugin permet de montrer l'utilisation des \em tags \b Doxygen
 * @author      Matthieu Jacquemet <Matthieu.jacquemet@univ-lyon1.fr>
 * @date        2021
 */
class EditorPlugin: public QObject {

protected:
    EditorPlugin() = default;

public:
    /**
     * @brief renvoie le label du widget de l'éditeur.
     */
    virtual QString label() = 0;

    /**
     * @brief renvoie le pointeur vers le widget de l'éditeur.
     */
    virtual QWidget* widget() = 0;

    virtual ~EditorPlugin() = default;
};


Q_DECLARE_INTERFACE(EditorPlugin, "org.alluvion.editor")


#endif // __EDITORPLUGIN_H__