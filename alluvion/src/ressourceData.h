// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __RESSOURCEDATA_H__
#define __RESSOURCEDATA_H__

#include "utils.h"
#include "singleton.h"
#include "terrainStage.h"


class DeclarativeStage;


template<class T>
class Ressource: public TerrainStage::PortData {

public:
    Ressource() = default;
    Ressource(const T& value);
    virtual ~Ressource() = default;

    virtual void set(QCLKernel& kernel, int index) override;
    virtual void setup(Terrain& terrain) override;
    virtual QtNodes::NodeDataType type() const override;

    virtual QJsonValue serialize() const;
    virtual void deserialize(const QJsonValue& value);

    T value;
};



class RessourceFactory: private Singleton<RessourceFactory> {

public:
    ~RessourceFactory() = default;

    typedef TerrainStage::DataPtr Creator();

    template<class T> static void registerRessource(const QString& id);
    template<class T> static QtNodes::NodeDataType& ressourceID();
    
    static TerrainStage::DataPtr createRessource(const QString& id);
    static TerrainStage::DataPtr getDefault(const QString& id);

    static void setupDefaults(Terrain& terrain);

private:
    RessourceFactory() = default;
    Q_DISABLE_COPY_MOVE(RessourceFactory)

    struct Entry {
        TerrainStage::DataPtr default_data;
        Creator* creator;
    };

    typedef QMap<QString, Entry> Entries;
    Entries entries;

    friend class Singleton<RessourceFactory>;
};


#define REGISTER_RESSOURCE_TYPE(TYPE) \
    INITIALIZER(res) { RessourceFactory::registerRessource<TYPE>(#TYPE); }


extern const QCLImageFormat default_format;


// REGISTER_RESSOURCE_TYPE("memory_object", QCLMemoryObject)
// REGISTER_RESSOURCE_TYPE("vector", QCLVectorBase)
// REGISTER_RESSOURCE_TYPE("buffer", void*)



// template<class T, const char* ID>
// class Ressource: public RessourceBase<T, ID> {

// public:
//     Ressource(const QJsonObject& data, const DeclarativeStage& stage);
//     virtual ~Ressource() = default;

// protected:
//     void setup(const QJsonObject& data, const DeclarativeStage& stage);
// };



#include "ressourceData.T"

#endif // __RESSOURCEDATA_H__