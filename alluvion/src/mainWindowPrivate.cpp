// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QLineEdit>
#include <QHBoxLayout>
#include <QPushButton>
#include <QInputDialog>
#include <QMessageBox>

#include "widgets.h"
#include "editorPlugin.h"
#include "pluginManager.h"
#include "utils.h"
#include "application.h"
#include "mainWindow.h"
#include "mainWindowPrivate.h"
#include "config.h"
#include "nodeEditor/nodeEditor.h"


#ifdef Q_OS_WIN
	#include <windows.h>
	#include <psapi.h>
#elif defined(Q_OS_LINUX) || defined (Q_OS_DARWIN)
	#include <unistd.h>
#endif


using namespace ads;
using namespace std;



QString getSystemMemory(StatusEntry* entry) {

	QLocale locale = entry->locale();
	size_t usage = 0;
	QString result("Memory: %1");

#ifdef Q_OS_WIN

	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), 
		(PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));

	usage = (size_t)pmc.WorkingSetSize;

#elif defined(Q_OS_LINUX)

	QFile file("/proc/self/statm");

	if (file.open(QIODevice::ReadOnly)) {

		QString line = file.readLine();
		usage = line.split(" ")[1].toUInt();
		usage *= getpagesize();
		file.close();
	}
#endif

	return result.arg(locale.formattedDataSize(usage));
}


QString getVideoMemory(StatusEntry* entry) {

	QLocale locale = entry->locale();
	QString result("VRAM : %1");
	QString value = "n/a";

	if (GLEW_NVX_gpu_memory_info) {

		int total_mem = 0, free_mem = 0;
		glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &total_mem);
		glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &free_mem);
		
		QString size = locale.formattedDataSize(qint64(total_mem) * 1024);
	
		float total, usage;
		QTextStream(&size) >> total;
		usage = float(total_mem - free_mem) / float(total_mem) * total;

		value = QString::number(usage, 'g', 3) + "/" + size;
	}
	else if (GLEW_ATI_meminfo) {

		int stats[4];
		glGetIntegerv(GL_TEXTURE_FREE_MEMORY_ATI, stats);

		value = locale.formattedDataSize(stats[0] * 1024) + " free";
	}

	return result.arg(value);
}


QString getSceneStatistics(StatusEntry* entry) {

	Application* app = Application::globalInstance();

	QString result("Vert: %1/Tris: %2");
	int nvert = 0, ntris = 0;

	for (const Mesh& mesh: app->objects.values()) {
		nvert += mesh.Vertexes();
		ntris += mesh.Triangles();
	}

	return result.arg(nvert).arg(ntris);
}


QString getVersion(StatusEntry* entry) {

	return "v" + Application::applicationVersion();
}



MainWindowPrivate::MainWindowPrivate(MainWindow* window):
	status_bar(window),
	q_ptr(window),
	select_layout(dock_manager)
{
	dock_manager.setStyleSheet("");
}


void MainWindowPrivate::setupUI() {

	q_ptr->setCentralWidget(&dock_manager);
	q_ptr->setStatusBar(&status_bar);

	q_ptr->toolBar->addWidget(makeSpacer());
	q_ptr->toolBar->addWidget(&select_layout);

	q_ptr->menuView->addMenu(&status_bar.menu);
	q_ptr->menuView->addMenu(select_layout.menu);

	q_ptr->actionSave_Copy->setEnabled(false);
}


void MainWindowPrivate::loadLayouts(const QByteArray& data) {
	select_layout.deserialize(data);
}

QByteArray MainWindowPrivate::saveLayouts() const {
	return select_layout.serialize();
}


void MainWindowPrivate::createEditors() {

	PluginManager* mgr = PluginManager::globalInstance();
	auto plugins = mgr->getPlugins<EditorPlugin>();

	for (QSharedPointer<EditorPlugin>& plugin: plugins) {
		ads::CDockWidget* dock = new ads::CDockWidget(plugin->label());
		
		dock->setWidget(plugin->widget(), ads::CDockWidget::ForceNoScrollArea);

    	dock_manager.addDockWidget(DockWidgetArea::CenterDockWidgetArea, dock);
    	q_ptr->menuView->addAction(dock->toggleViewAction());
	}
	// EditorPlugin* plugin = mgr->getPlugin<EditorPlugin>("NodeEditorPlugin");
	// if (NodeEditorPlugin* viewport = qobject_cast<NodeEditorPlugin*>(plugin)) {
	// 	q_ptr->setCentralWidget(viewport->widget());
	// 	viewport->widget()->setProperty("dockWidgetContent", true);
	// }
}


void MainWindowPrivate::initStatusBar() {

	status_bar.addStatusEntry(tr("Video Memory"), getVideoMemory);
	status_bar.addStatusEntry(tr("System Memory"), getSystemMemory);
	status_bar.addStatusEntry(tr("Scene Statistics"), getSceneStatistics);
	status_bar.addStatusEntry(tr("Alluvion Version"), getVersion, 0);
}



StatusBar::StatusBar(MainWindow* window):
	QStatusBar(window),
	menu(tr("Status Bar"), this)
{
	setContextMenuPolicy(Qt::CustomContextMenu);
	addWidget(makeSpacer());

	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), 
        	this, SLOT(showContextMenu(const QPoint&)));
}



void StatusBar::addStatusEntry(const QString& name, UpdateFunc func, int interval) {

	StatusEntry* entry = new StatusEntry(name, qMove(func), interval);
	layout()->addItem(new QWidgetItem(entry));

	entry->setParent(this);
	entries.append(entry);
	menu.addAction(&entry->action);
}


void StatusBar::showContextMenu(const QPoint &pos) {

	menu.exec(mapToGlobal(pos));
}


StatusEntry::StatusEntry(const QString& name, UpdateFunc func, int interval):
	QLabel(name),
	action(name, this),
	callback(func)
{

	action.setCheckable(true);
	action.setChecked(true);
	connect(&action, SIGNAL(triggered(bool)), this, SLOT(setVisible(bool)));

	if (interval > 0) {
		timer.setInterval(1000);
		connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
		timer.start();

	} else update();
}


void StatusEntry::update() {

	setText(callback(this));
	QLabel::update();
}





INITIALIZER(main_window) {

	CDockManager::setConfigFlag(CDockManager::OpaqueSplitterResize);
    CDockManager::setConfigFlag(CDockManager::XmlCompressionEnabled);
    CDockManager::setConfigFlag(CDockManager::FocusHighlighting);
	CDockManager::setConfigFlag(CDockManager::DragPreviewIsDynamic);
	CDockManager::setConfigFlag(CDockManager::DragPreviewShowsContentPixmap);
	CDockManager::setConfigFlag(CDockManager::DragPreviewHasWindowFrame);
	// CDockManager::setConfigFlag(CDockManager::FloatingContainerForceNativeTitleBar);
}

























// SelectLayoutWidget::Layouts::iterator
// SelectLayoutWidget::createLayout(const QString& name, const QByteArray& data) {

// 	if (Q_UNLIKELY(layouts.size() == 1))
// 		layouts.begin()->remove->setDisabled(false);

// 	auto it = layouts.insert(layouts.end(), {name, data});

// 	menu->insertAction(separator, it->select);
// 	remove_menu->addAction(it->remove);

// 	connect(it->remove, &QAction::triggered, [=] {
// 		setCurrent(removeLayout(it));
// 	});

// 	connect(it->select, &QAction::triggered, [=] {
// 		if (current != layouts.end())
// 			current->data = mgr.saveState();
// 		setCurrent(it);
// 	});

// 	return it;
// }


// SelectLayoutWidget::Layouts::iterator
// SelectLayoutWidget::removeLayout(Layouts::iterator it) {
	
// 	remove_menu->removeAction(it->remove);
// 	menu->removeAction(it->select);

// 	it = layouts.erase(it);

// 	if (it != layouts.begin())
// 		it = std::prev(it);

// 	if (layouts.size() == 1)
// 		it->remove->setDisabled(true);

// 	return it;
// }


// void SelectLayoutWidget::setCurrent(Layouts::iterator it) {

// 	current = it;

// 	if (Q_LIKELY(current != layouts.end())) {
// 		setText(current->select->text());
// 		current->data.isEmpty() || mgr.restoreState(current->data);
// 	}
// }


// void SelectLayoutWidget::clear() {

// 	Layouts::iterator it = layouts.begin();

// 	while (it != layouts.end())
// 		it = removeLayout(it);
// }


// QByteArray SelectLayoutWidget::serialize() const {

// 	if (current != layouts.end())
// 		current->data = mgr.saveState();

// 	QVector<QPair<QString, QByteArray>> items;

// 	for (const Layout& layout : layouts) {
// 		QString name = layout.select->text();
// 		items.append({qMove(name), layout.data});
// 	}

// 	QByteArray buffer;
// 	QDataStream out(&buffer, QIODevice::WriteOnly);
// 	out << items << (int)std::distance(layouts.begin(), current);

// 	return buffer;
// }


// void SelectLayoutWidget::deserialize(const QByteArray& buffer) {

// 	clear();

// 	int id; QVector<QPair<QString, QByteArray>> items;
// 	QDataStream in(buffer);
// 	in >> items >> id;

// 	for (const QPair<QString, QByteArray>& item: items)
// 		createLayout(item.first, item.second);

// 	setCurrent(std::next(layouts.begin(), id));
// }


// void SelectLayoutWidget::addLayout() {

// 	QString new_name = getUnique("Layout");
// 	QString name;
// 	bool ok = false;

//  	do {
//         name = QInputDialog::getText(Q_NULLPTR, "New Layout", "",
// 								QLineEdit::Normal, new_name, &ok);
// 		if (!ok) return;

// 	} while (name.isEmpty());


// 	if (current != layouts.end())
// 		current->data = mgr.saveState();

// 	setCurrent(createLayout(name));
// }



// QString SelectLayoutWidget::getUnique(const QString& name) const {
	
// 	QString new_name = name;
// 	QStringList names;
// 	QString fmt("%1.%2");
// 	int i = 0;

// 	for (const Layout& layout: layouts)
// 		names.append(layout.select->text());

// 	while (names.contains(new_name))
// 		new_name = fmt.arg(name).arg(i++, 3, 10, QChar('0'));
	
// 	return new_name;
// }



















// SelectLayoutWidget::SelectLayoutWidget(CDockManager& _mgr):
// 	combo(new ComboBox(this)),
// 	add_btn(new QPushButton(this)),
// 	del_btn(new QPushButton(this)),
// 	current(-1),
// 	mgr(_mgr)
// {
// 	combo->setView(new QListView);
// 	combo->

// 	setLayout(new QHBoxLayout);
// 	layout()->setContentsMargins(0,0,0,0);
// 	layout()->setSpacing(0);

// 	layout()->addWidget(combo);
// 	layout()->addWidget(add_btn);
// 	layout()->addWidget(del_btn);

// 	combo->setFocusPolicy(Qt::ClickFocus);
// 	combo->setInsertPolicy(QComboBox::NoInsert);
// 	combo->setToolTip("layout");
// 	combo->setEditable(true);
// 	combo->clearFocus();

// 	add_btn->setObjectName("add_layout");
// 	del_btn->setObjectName("del_layout");

// 	setupActions();

// 	QTimer::singleShot(0, [this]() {
//         createRequested();
//     });
// }


// void SelectLayoutWidget::setupActions() {

// 	connect(add_btn, SIGNAL(pressed()),
// 			this, SLOT(createRequested()));

// 	connect(del_btn, SIGNAL(pressed()),
// 			this, SLOT(removeRequested()));

// 	connect(combo, SIGNAL(activated(int)),
// 			this, SLOT(changeRequested()));

// 	connect(combo->lineEdit(), SIGNAL(returnPressed()),
// 			this, SLOT(renameRequested()));
// }



// QByteArray SelectLayoutWidget::serialize() const {
	
// 	combo->setItemData(combo->currentIndex(), mgr.saveState());

// 	QVector<QPair<QString, QByteArray>> items;
// 	items.reserve(combo->count());

// 	for (int i=0; i<combo->count(); ++i) {
// 		QByteArray data = combo->itemData(i).toByteArray();
// 		items.append({combo->itemText(i), qMove(data)});
// 	}

// 	QByteArray buffer;
// 	QDataStream out(&buffer, QIODevice::WriteOnly);
// 	out << items << current;

// 	return buffer;
// }


// void SelectLayoutWidget::deserialize(const QByteArray& buffer) {

// 	combo->clear();
// 	QVector<QPair<QString, QByteArray>> items;

// 	QDataStream in(buffer);
// 	in >> items >> current;

// 	for (const QPair<QString, QByteArray>& item: items)
// 		combo->addItem(item.first, item.second);

// 	setCurrentLayout(current);
// }


// void SelectLayoutWidget::setCurrentLayout(int index) {

// 	QVariant data = combo->itemData(index);
// 	mgr.restoreState(data.toByteArray());
// 	combo->setCurrentIndex(index);
// 	current = index;
// }


// void SelectLayoutWidget::removeRequested() {
	
// 	if (combo->count() == 2)
// 		del_btn->setDisabled(true);

// 	combo->removeItem(combo->currentIndex());
// 	setCurrentLayout(combo->currentIndex());
// }


// void SelectLayoutWidget::createRequested() {
	
// 	if (combo->count() == 1)
// 		del_btn->setEnabled(true);

// 	combo->setItemData(current, mgr.saveState());

// 	combo->addItem(getUnique("layout"));
// 	combo->setCurrentIndex(combo->count() - 1);
// 	current = combo->currentIndex();
// }


// void SelectLayoutWidget::changeRequested() {

// 	combo->clearFocus();

// 	combo->setItemData(current, mgr.saveState());
// 	setCurrentLayout(combo->currentIndex());
// }


// void SelectLayoutWidget::renameRequested() {

// 	QString new_name = combo->lineEdit()->text();

// 	if (new_name != combo->itemText(current))
// 		new_name = getUnique(new_name);

// 	combo->setItemText(combo->currentIndex(), qMove(new_name));
// 	combo->clearFocus();
// }



// QString SelectLayoutWidget::getUnique(const QString& name) const {
	
// 	QString new_name = name;
// 	QString fmt("%1.%2");
// 	int i=0;

// 	while (combo->findText(new_name) != -1)
// 		new_name = fmt.arg(name).arg(i++, 3, 10, QChar('0'));

// 	return new_name;
// }






