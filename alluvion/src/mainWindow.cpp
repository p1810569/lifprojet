// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "application.h"
#include "mainWindow.h"
#include "mainWindowPrivate.h"


MainWindow::MainWindow():
	d_ptr(new MainWindowPrivate(this))
{
	setFocusPolicy(Qt::ClickFocus);
	setupUi(this);
}


void MainWindow::init() {

	d_ptr->setupUI();
	d_ptr->createEditors();
	d_ptr->initStatusBar();

	createActions();
	createShortcuts();
	showMaximized();
}


void MainWindow::createActions() {
	
	Application* app = Application::globalInstance();

	connect(actionOpen, SIGNAL(triggered(bool)),
			app, SLOT(loadProject()));

	connect(actionSave, SIGNAL(triggered(bool)),
			app, SLOT(saveProject()));

	connect(actionSave_As, SIGNAL(triggered(bool)),
			app, SLOT(saveProjectAs()));

	connect(actionSave_Copy, SIGNAL(triggered(bool)),
		app, SLOT(saveCopy()));
}


void loadShortcut(QAction* action, const QString& id, const QString& def) {

	Application* app = Application::globalInstance();

	QVariant var = app->settings.value("shortcuts/" + id, def);
	action->setShortcut(QKeySequence(var.toString()));
}



void MainWindow::createShortcuts() {

	loadShortcut(actionOpen, "open project", "ctrl+o");
	loadShortcut(actionSave, "save project", "ctrl+s");
	loadShortcut(actionSave_As, "save project as", "shift+ctrl+s");
	loadShortcut(actionOpen_Recent, "open recent project", "shift+ctrl+o");
	loadShortcut(actionSave_Copy, "save project", "");
	loadShortcut(actionQuit, "quit", "ctrl+q");
	loadShortcut(actionUndo, "undo", "ctrl+z");
	loadShortcut(actionRedo, "redo", "ctrl+shift+z");
	loadShortcut(actionSave_Screenshot, "screenshot", "");
}


MainWindowPrivate* MainWindow::impl() const {
	return d_ptr.get();	
}

void MainWindow::loadLayouts(const QByteArray& data) {
	d_ptr->loadLayouts(data);
}

QByteArray MainWindow::saveLayouts() const {
	return d_ptr->saveLayouts();
}
   

