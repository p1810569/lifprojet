// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "texture.h"

using namespace std;


Texture::Texture(const string& name):
    name(name),
    wrap_u(WM_clamp),
    wrap_v(WM_clamp),
    wrap_w(WM_clamp),
    min_filter(SF_linear),
    mag_filter(SF_linear),
    anisotropy(0),
    lod_bias(0.0f),
    min_lods(-1000.0f),
    max_lods( 1000.0f),
    color(Color(0.0f)),
    component(CT_unsigned_byte),
    format(F_rgba_8)
{
    size[0] = 1;
    size[1] = 1;
    size[2] = 1;
}


void Texture::loadFromFile(QString filename) {
    // TODO    
}


const unsigned char* Texture::getData() const {
    
    if (data.size() > 0)
        return data.data();

    size_t new_size = size[0] * size[1] * size[2];
    new_size *= getNumComponents(format);
    new_size *= getComponentSize(component);

    data.resize(new_size);

    return data.data();
}


bool Texture::isUnsigned(ComponentType type) {
    
    return  type == CT_unsigned_byte ||
            type == CT_unsigned_int ||
            type == CT_unsigned_int_24_8 ||
            type == CT_unsigned_short;
}


int Texture::getComponentSize(ComponentType type) {

    switch (type) {
        case CT_unsigned_byte:
        case CT_byte:
            return 1;

        case CT_unsigned_short:
        case CT_short:
        case CT_half_float:
            return 2;

        case CT_float:
        case CT_unsigned_int_24_8:
        case CT_int:
        case CT_unsigned_int:
            return 4;
    }
    return 1;
}


int Texture::getNumComponents(Format format) {

    switch (format) {
        case F_depth_stencil:
        case F_depth:
        case F_depth_16:
        case F_depth_24:
        case F_depth_32:
        case F_r:
        case F_g:
        case F_b:
        case F_a:
        case F_r_16:
        case F_r_32:
        case F_r_8i:
        case F_r_16i:
        case F_r_32i:
            return 1;

        case F_rg_8:
        case F_rg_16:
        case F_rg_32:
        case F_rg_8i:
        case F_rg_16i:
        case F_rg_32i:
            return 2;

        case F_rgb:
        case F_rgb_8:
        case F_rgb_16:
        case F_rgb_32:
        case F_rgb_8i:
        case F_rgb_16i:
        case F_rgb_32i:
            return 3;

        case F_rgba_8:
        case F_rgba_16:
        case F_rgba_32:
        case F_rgba_8i:
        case F_rgba_16i:
        case F_rgba_32i:
            return 4;
    }
    return 1;
}