// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include "application.h"
#include "utils.h"



QWidget* makeSpacer() {

	QWidget* spacer = new QWidget();
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    return spacer;
}



EnumInfo getEnums(const QMetaObject& meta_class, const QString& id) {
    
    int index = meta_class.indexOfEnumerator(id.toUtf8().data());
    EnumInfo result;

    if (Q_LIKELY(index != -1)) {

        QMetaEnum meta_enum = meta_class.enumerator(index);
        int count = meta_enum.keyCount();
    
        for (int i=0; i<count; ++i)
            result.append({ meta_enum.value(i),
                            meta_enum.key(i)});
    }
    return result;
}
