// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <nodes/Node>

#include "ressourceData.h"
#include "application.h"
#include "terrain.h"
#include "stages/noiseStage.h"


using namespace std;
using namespace QtNodes;


QCLDevice::DeviceType getDeviceType(const QString& name) {

    if (name == "default")
        return QCLDevice::Default;
    else if (name == "cpu")
        return QCLDevice::CPU;
    else if (name == "gpu")
        return QCLDevice::GPU;
    else if (name == "accelerator")
        return QCLDevice::Accelerator;
    else if (name == "all")
        return QCLDevice::All;
    else
        qWarning() << "invalid device type : " << name;

    return QCLDevice::Default;
}



Terrain::Terrain(Application* app) {
    
    QVariant type_value = app->settings.value("solver/device", "cpu");
    QVariant size_value = app->settings.value("solver/size", 1024);

    dev_type = getDeviceType(type_value.toString());

    setGridSize({size_value.toInt(), size_value.toInt()});

    connect(this, &Terrain::nodeCreated, this, &Terrain::setupNode);
    connect(this, &Terrain::nodeDeleted, this, &Terrain::destroyNode);

    setupContext();
}


void Terrain::setupNode(Node& node) {

    NodeDataModel* model = node.nodeDataModel();
    TerrainStage* stage = qobject_cast<TerrainStage*>(model);

    if (stage != Q_NULLPTR)
        stage->nodeCreated(node);
}


void Terrain::destroyNode(Node& node) {
    
    NodeDataModel* model = node.nodeDataModel();
    TerrainStage* stage = qobject_cast<TerrainStage*>(model);

    if (stage != Q_NULLPTR)
        stage->has_delete = true;
}


void Terrain::setupContext() {
    
    if (Q_LIKELY(context.create(dev_type))) {

        RessourceFactory::setupDefaults(*this);
        Q_EMIT contextChanged(context);
    } else {
        qCritical() << "failed to create opencl context, aborting";
        exit(EXIT_FAILURE);
    }
}


void Terrain::setGridSize(const QSize& _grid_size) {
    grid_size = _grid_size;
    
    setProperty("width", grid_size.width());
    setProperty("height", grid_size.height());
}


void Terrain::setDeviceType(QCLDevice::DeviceType type) {
    
    dev_type = type;
    setupContext();
}


QSize Terrain::size() const {
    return grid_size;
}


Mesh Terrain::mesh() const {

    size_t width = grid_size.width();
    size_t height = grid_size.height();

    size_t nvert_x = width + 1;
    size_t nvert_y = height + 1;

    QVector<Vector> vertices(nvert_x * nvert_y);
    QVector<Vector> normals(1, Vector::Z);

    QVector<int> narray(width * height * 6, 0);
    QVector<int> varray;

    varray.reserve(narray.size());


    // remplissage de la grille de vertices
    for (size_t i=0; i<=height; ++i) {
        float y = float(i) / height - 0.5f;

        for (size_t j=0; j<=width; ++j) {
            float x = float(j) / width - 0.5f;
            vertices[i * nvert_y + j] = Vector(x, y, 0.0f);
        }
    }

    // ajout de deux triangles par cellules de la grille
    for (size_t y=0; y<height; ++y) {
        for (size_t x=0; x<width; ++x) {

            int ul = y * nvert_x + x;
            int ur = ul + 1;
            int ll = ul + nvert_x;
            int lr = ur + nvert_x;

            varray.append({ul, ll, lr});
            varray.append({ul, lr, ur});
        }
    }

    return Mesh(vertices, normals, varray, narray);
}
