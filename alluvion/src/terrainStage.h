// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __TERRAINSTAGE_H__
#define __TERRAINSTAGE_H__


#include <QString>
#include <QVector>
#include <QSharedPointer>

#include <nodes/NodeDataModel>

#include "qclimage.h"
#include "buffer.h"

class Terrain;
class ComputeTask;
class QCLContext;
class QCLKernel;


#define DECLARE_INPUT(name, type) \
    PortEntry& name = defineInput(#name, type);

#define DECLARE_OUTPUT(name, type) \
    PortEntry& name = defineOutput(#name, type);


class TerrainStage: public QtNodes::NodeDataModel {

    Q_OBJECT

public:
    TerrainStage(Terrain& terrain, const QString& name="");
    virtual ~TerrainStage() = default;

    virtual QString caption() const override;
    virtual QString name() const override;

    virtual unsigned int nPorts(QtNodes::PortType portType) const override;

    virtual void setInData(std::shared_ptr<QtNodes::NodeData> nodeData,
                            QtNodes::PortIndex port) override;

    virtual QtNodes::NodeDataType dataType(
                            QtNodes::PortType portType,
                            QtNodes::PortIndex portIndex) const override;

    virtual std::shared_ptr<QtNodes::NodeData> outData(
                            QtNodes::PortIndex port) override;

    virtual QWidget* embeddedWidget() override;

    virtual void compute(QCLContext& context) = 0;

    virtual void nodeCreated(QtNodes::Node& node);

    class PortData: public QtNodes::NodeData {
    public:
        PortData() = default;
        virtual ~PortData() = default;

        virtual void set(QCLKernel& kernel, int index) = 0;
        virtual void setup(Terrain& terrain) = 0;

        virtual QJsonValue serialize() const = 0;
        virtual void deserialize(const QJsonValue& value) = 0;

        template<class T> bool isOfType() const;
    };
    typedef std::shared_ptr<PortData> DataPtr;


    struct PortEntry: public QtNodes::NodeDataType {
        PortEntry() = default;
        PortEntry(const QString& name, const QString& type);

        DataPtr data;
    };
    typedef QVector<PortEntry> PortEntries;

    Terrain& terrain;
    bool has_delete;

public Q_SLOTS:
    virtual void setup(QCLContext& context);

protected:
    PortEntries inputs, outputs;
    QString label, version, description;

    const PortEntries& getPorts(QtNodes::PortType portType) const;

    PortEntry& defineInput(const QString& name, const QString& type);
    PortEntry& defineOutput(const QString& name, const QString& type);

public Q_SLOTS:
    virtual void terrainSizeChaged(size_t width, size_t height);
};


class StageConverter: public QObject {

    Q_OBJECT
public:
    StageConverter(Terrain& terrain);

    virtual ~StageConverter() = default;

protected Q_SLOTS:
    virtual void setup(QCLContext& context);
};


class EmbeddedWidget: public QWidget {

    Q_OBJECT

public:
    EmbeddedWidget(QWidget* parent=Q_NULLPTR);
    virtual ~EmbeddedWidget() = default;
};

#endif // __TERRAINSTAGE_H__