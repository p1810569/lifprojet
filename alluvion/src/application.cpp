// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QFileDialog>
#include <QDebug>
#include <QStandardPaths>

#include "utils.h"
#include "pluginManager.h"
#include "config.h"
#include "mainWindow.h"
#include "application.h"


Application::Callbacks Application::callbacks;



Application::Application(int& argc, char** argv):
    QApplication(argc, argv),
    terrain(this)
{
    parseOptions();
    loadStyleSheet();
    init();
}


void Application::parseOptions() {

#ifdef Q_OS_LINUX
    static const QString plugin_path = "/usr/lib/" APP_NAME "/plugins";
#else
    static const QString plugin_path = relativePath("plugins");
#endif

    static const QString APP_DESC = Application::translate("main",
        "Alluvion is a corss-platform graph based"
        "procedural terrain generator.");

    static const QCommandLineOption OPT_PLUGIN = {
        {"p","plugin-path"},
        QCoreApplication::translate("main","Path to the plugin directory."),
        "plugin_path",
        settings.value("plugin/path", plugin_path).toString()
    };

    parser.setApplicationDescription(APP_DESC);

    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(OPT_PLUGIN);

    parser.process(*this);
}


void Application::init() {

    QString plugin_path = parser.value("plugin-path");

    if (!plugin_path.isEmpty()) {
        PluginManager* mgr = PluginManager::globalInstance();
        mgr->loadPlugins(plugin_path);
    }

    objects["terrain"] = terrain.mesh();
    window.init();

    for (const CreateCallback& callback: callbacks)
        callback(this);

    callbacks.clear();

///////////////////// ONLY FOR DEVELOPMENT //////////////////////

	if (!watcher.addPath(relativePath("../../ressources/styles/default.qss")))
		qDebug() << "failed to add file to watcher";
	else
		connect(&watcher, SIGNAL(fileChanged(const QString&)),
				this, SLOT(reloadStyleSheet(const QString&)));

//////////////////////////////////////////////////////////////////
}


Application* Application::globalInstance() {
    return static_cast<Application*>(instance());
}


QString Application::relativePath(const QString& name) {

    QDir dir(applicationDirPath());
    return dir.absoluteFilePath(name);
}


void Application::loadStyleSheet() {

	QFile file(":/styles/default.qss");

	if (file.open(QIODevice::ReadOnly)) {
		setStyleSheet(file.readAll());
		file.close();
	}
}

///////////////////// ONLY FOR DEVELOPMENT //////////////////////

void Application::reloadStyleSheet(const QString& path) {
	
	QFile file(path);

	if (file.open(QIODevice::ReadOnly)) {
		setStyleSheet(file.readAll());
		file.close();
	}
}

void Application::saveProjectData(const QString& path) {

    QFile file(path);

    if (file.open(QIODevice::WriteOnly)) {

        QByteArray layout_data = window.saveLayouts();
        QByteArray terrain_data = terrain.saveToMemory();

        QDataStream out(&file);
        out << terrain_data << layout_data;

        file.close();
    }
}

void Application::onCreated(CreateCallback callback) {

    if (startingUp())
        callbacks.append(callback);
    else
        callback(globalInstance());
}


void Application::loadProject() {
    
    QString path = QStandardPaths::writableLocation(
        QStandardPaths::DocumentsLocation);

    QString filename = QFileDialog::getOpenFileName(Q_NULLPTR,
                                 tr("Open Project"), path,
                                 tr("Alluvion Project Files (*.alv)"));
    
    QFile file(filename);

    if (file.open(QIODevice::ReadOnly)) {
        current_project = filename;

        QByteArray terrain_data, layout_data;
        QDataStream in(&file);

        in >> terrain_data >> layout_data;

        terrain.clearScene();

        window.loadLayouts(layout_data);
        terrain.loadFromMemory(terrain_data);

        file.close();
    }
    Q_EMIT projectOpened(current_project);
}


void Application::saveProject() {

    if (QFileInfo::exists(current_project))
        saveProjectData(current_project);
    else
        saveProjectAs();
}


void Application::saveProjectAs() {
    
    QString path = QStandardPaths::writableLocation(
        QStandardPaths::DocumentsLocation);

    QString filename = QFileDialog::getSaveFileName(Q_NULLPTR,
                                 tr("Save project as"), path,
                                 tr("Alluvion Project Files (*.alv)"));

    if (filename.isEmpty())
        return;

    if (!filename.endsWith("alv", Qt::CaseInsensitive))
        filename += ".alv";

    saveProjectData(filename);
    current_project = filename;
}


void Application::saveCopy(){

    if (!QFileInfo::exists(current_project)) {
        saveProjectAs();
        return;
    }

    QFileInfo filename(current_project);
    QString abspath = filename.dir().absoluteFilePath(filename.baseName());
    QString fmt = abspath + ".%1.alv";
    QString new_path;
	int i = 0;

    do
        new_path = fmt.arg(i++, 3, 10, QChar('0'));
    while (QFileInfo::exists(new_path));
    
    saveProjectData(new_path);
}

//////////////////////////////////////////////////////////////////



INITIALIZER(application) {

    QLocale::setDefault(QLocale::c());

	Application::setOrganizationName(ORG_NAME);
	Application::setApplicationName(APP_NAME);
    Application::setApplicationVersion(APP_VERSION);

	Application::setAttribute(Qt::AA_ShareOpenGLContexts, true);

}
