// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __BUFFER_H__
#define __BUFFER_H__

#include <QtCore/QVector>


class BufferBase {

public:
    ~BufferBase() = default;

    virtual void resize(size_t width, size_t height) = 0;

    size_t getWidth() const;
    size_t getHeight() const;

protected:
    BufferBase(size_t width, size_t height);

    size_t width, height;
};


template<class T>
class Buffer: public BufferBase {

public:
    Buffer(size_t width, size_t height);
    ~Buffer() = default;

    T& get(size_t x, size_t y);
    void set(size_t x, size_t y, T value);

    virtual void resize(size_t width, size_t height) override;

private:
    typedef QVector<T> Data;
    Data data;
};


#include "buffer.T"

#endif // __BUFFER_H__