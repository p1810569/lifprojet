// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QtCore/QThreadPool>

#include "terrain.h"
#include "terrainStage.h"
#include "application.h"

using namespace std;
using namespace QtNodes;



/**
 * @file        terrainStage.cpp
 * @brief       Contient la définition de la classe TerrainStage
 * @details     La classe \c TerrainStage permet de montrer l'utilisation des \em tags \b Doxygen
 * @author      Matthieu Jacquemet <Matthieu.jacquemet@univ-lyon1.fr>
 * @date        2021
 */

/**
 * @brief       Constructeur de la classe TerrainStage
 * @param       _terrain Terrain
 * @param       name Données sérialisées       
 */ 
TerrainStage::TerrainStage(Terrain& _terrain, const QString& name):
    label(name),
    terrain(_terrain),
    has_delete(false)
{
    connect(&terrain, SIGNAL(contextChanged(QCLContext&)),
            this, SLOT(setup(QCLContext&)));
}



void TerrainStage::setup(QCLContext& context) {

}


QString TerrainStage::caption() const {
    return label;
}


QString TerrainStage::name() const {
    return label;
}


unsigned int TerrainStage::nPorts(PortType portType) const {
    
    const PortEntries& ports = getPorts(portType);
    return ports.size();
}


NodeDataType TerrainStage::dataType(PortType portType, PortIndex portIndex) const {

    const PortEntries& ports = getPorts(portType);
    Q_ASSERT(portIndex < ports.size());

    return ports[portIndex];
}


void TerrainStage::setInData(std::shared_ptr<NodeData> nodeData, PortIndex port) {
    Q_ASSERT(port < inputs.size());
    inputs[port].data = dynamic_pointer_cast<PortData>(nodeData);

    if (Q_LIKELY(!has_delete))
        compute(terrain.context);
    else {
        terrain.context.flush();
        terrain.context.finish();
    }
}


std::shared_ptr<NodeData> TerrainStage::outData(PortIndex port) {
    Q_ASSERT(port < outputs.size());
    return outputs[port].data;
}


QWidget* TerrainStage::embeddedWidget() {
    return Q_NULLPTR;
}


void TerrainStage::nodeCreated(QtNodes::Node& node) {
    setup(terrain.context);
}


const TerrainStage::PortEntries& TerrainStage::getPorts(PortType portType) const {
    
    switch (portType) {
        case PortType::In: return inputs;
        case PortType::Out: return outputs;
        default: break;
    }
    return inputs;
}




TerrainStage::PortEntry& TerrainStage::defineInput( const QString& name,
                                                    const QString& type)
{
    inputs.append({name, type});
    return inputs.back();
}


TerrainStage::PortEntry& TerrainStage::defineOutput(const QString& name, 
                                                    const QString& type)
{
    outputs.append({name, type});
    return outputs.back();
}


void TerrainStage::terrainSizeChaged(size_t width, size_t height) {

}


StageConverter::StageConverter(Terrain& terrain) {

    connect(&terrain, SIGNAL(contextChanged(QCLContext&)),
            this, SLOT(setup(QCLContext&)));
}



void StageConverter::setup(QCLContext& context) {
    
}



EmbeddedWidget::EmbeddedWidget(QWidget* parent):
    QWidget(parent)
{
    setFocusPolicy(Qt::ClickFocus);    
}


TerrainStage::PortEntry::PortEntry(const QString& name, const QString& id):
    QtNodes::NodeDataType({id, name})
{

}


// TerrainStage::PortEntry::operator const QCLMemoryObject&() const{

//     const QCLImage2D* ptr = data.get();

//     if (ptr == Q_NULLPTR)
//         return *ptr;
    
//     static QCLImage2D null;
//     return null;
// }

