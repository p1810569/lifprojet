// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <QtCore/QString>
#include "color.h"


class Texture {

public:
    explicit Texture(const std::string& name="");
    ~Texture();

    enum SamplingFilter {
        SF_nearest,
        SF_linear,
        SF_nearest_mipmap_nearest,
        SF_nearest_mipmap_linear,
        SF_linear_mipmap_nearest,
        SF_linear_mipmap_linear,
        SF_shadow
    };

    enum WrapMode {
        WM_clamp,
        WM_repeat,
        WM_mirror,
        WM_mirror_once,
        WM_border_color
    };

    enum Type {
        T_1d_texture,
        T_2d_texture,
        T_3d_texture,
        T_1d_texture_array,
        T_2d_texture_array,
    };

    enum Format {
        F_depth_stencil,
        F_depth,
        F_depth_16,
        F_depth_24,
        F_depth_32,

        F_r,
        F_g,
        F_b,
        F_a,
        F_rgb,

        F_rg_8,
        F_rgb_8,
        F_rgba_8,

        F_r_16,
        F_rg_16,
        F_rgb_16,
        F_rgba_16,

        F_r_32,
        F_rg_32,
        F_rgb_32,
        F_rgba_32,

        F_r_8i,
        F_rg_8i,
        F_rgb_8i,
        F_rgba_8i,

        F_r_16i,
        F_rg_16i,
        F_rgb_16i,
        F_rgba_16i,

        F_r_32i,
        F_rg_32i,
        F_rgb_32i,
        F_rgba_32i,
    };

    enum ComponentType {
        CT_unsigned_byte,
        CT_unsigned_short,
        CT_float,
        CT_unsigned_int_24_8,
        CT_int,
        CT_byte,
        CT_short,
        CT_half_float,
        CT_unsigned_int,
    };

    void loadFromFile(QString filename);

    static bool isUnsigned(ComponentType type);

    static int getComponentSize(ComponentType type);
    static int getNumComponents(Format format);

    const unsigned char* getData() const;

public:
    typedef std::vector<unsigned char> Data;
    
    Type type;
    std::string name;
    Format format;
    ComponentType component;
    size_t size[3];
    SamplingFilter min_filter;
    SamplingFilter mag_filter;
    int anisotropy;
    Color color;
    WrapMode wrap_u;
    WrapMode wrap_v;
    WrapMode wrap_w;
    float min_lods;
    float max_lods;
    float lod_bias;
    mutable Data data;
};

#endif // __TEXTURE_H__