// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SPINBOX_H__
#define __SPINBOX_H__

#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QLineEdit>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QComboBox>
#include <QSlider>
#include <ZtWidgets/slideredit.h>


class QListWidgetItem;
class QListWidget;


class ComboBoxWidget: public QWidget {

    Q_OBJECT

public:
    ComboBoxWidget(const QString& name, QListWidgetItem* item);
    virtual ~ComboBoxWidget() = default;
    
    void initUi();
    QString name() const;

Q_SIGNALS:
    void removeItem(QListWidgetItem* item);

private Q_SLOTS:
    void removeRequested();

protected:
    QString text;
    QListWidgetItem* item;
};


class ListComboBox: public QComboBox {

    Q_OBJECT

public:
    ListComboBox(QWidget* parent=Q_NULLPTR);
    virtual ~ListComboBox() = default;

    void addItem(const QString& entry);
    void removeItem(QListWidgetItem* item);

protected:
    QListWidget* list;

    QStringList itemNames() const;
};


class Slider: public SliderEdit {

    Q_OBJECT

public:
    Slider(QWidget* parent=Q_NULLPTR);
    virtual ~Slider() = default;

protected:
    virtual void mouseMoveEvent(QMouseEvent* event) override;
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseReleaseEvent(QMouseEvent* event) override;

private:
    bool is_pressing;
};


class ComboBox: public QComboBox {

    Q_OBJECT

public:
    ComboBox(QWidget* parent=Q_NULLPTR);
    virtual ~ComboBox() = default;

protected:
    virtual void showPopup() override;
};


class DoubleSpinBox: public QDoubleSpinBox {

    Q_OBJECT

public:
    DoubleSpinBox(QWidget* parent=Q_NULLPTR);
    virtual ~DoubleSpinBox() = default;
};

class IntSpinBox: public QSpinBox {

    Q_OBJECT

public:
    IntSpinBox(QWidget* parent=Q_NULLPTR);
    virtual ~IntSpinBox() = default;
};


class SpinBoxVectorBase: public QWidget {

    Q_OBJECT

public:
    SpinBoxVectorBase(uint8_t n, QWidget* parent=Q_NULLPTR);
    virtual ~SpinBoxVectorBase() = default;

    typedef QVector<DoubleSpinBox*> SpinBoxes;
    SpinBoxes spinboxes;
};



template<class SpinBoxT>
class DragLineEdit: public QLineEdit {

public:
    DragLineEdit(SpinBoxT& spinbox);
    ~DragLineEdit() = default;

protected:
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseMoveEvent(QMouseEvent* event) override;
    virtual void mouseReleaseEvent(QMouseEvent* event) override;
    virtual void focusInEvent(QFocusEvent* event) override;
    virtual void focusOutEvent(QFocusEvent* event) override;

    void finishDrag();
    void finishEdit();

    SpinBoxT& spinbox;
    bool is_dragging, is_editing, value_changed;
    QPoint prev_mouse_pos;
    double initial_value;
};


#define REGISTER_SPINBOX_VECTOR_TYPE(SIZE) \
    class SpinBoxVector ## SIZE ## D: public SpinBoxVectorBase { \
    public: \
        SpinBoxVector ## SIZE ## D(QWidget* parent=Q_NULLPTR): \
            SpinBoxVectorBase(SIZE##u, parent) {} \
        virtual ~SpinBoxVector ## SIZE ## D() = default; \
        \
        inline QVector ## SIZE ## D value() const { \
            QVector ## SIZE ## D vec; \
            for (uint8_t i=0; i<SIZE; ++i) vec[i] = spinboxes[i]->value(); \
            return vec; \
        } \
        inline void setValue(const QVector ## SIZE ## D& vec) const { \
            for (uint8_t i=0; i<SIZE; ++i) spinboxes[i]->setValue(vec[i]); \
        } \
    };


REGISTER_SPINBOX_VECTOR_TYPE(2)
REGISTER_SPINBOX_VECTOR_TYPE(3)
REGISTER_SPINBOX_VECTOR_TYPE(4)


#include "widgets.T"


#endif // __SPINBOX_H__