#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__


#include <QMainWindow>
#include <QAction>

#include "meshWidget.h"
#include "meshcolor.h"
#include "terrain.h"
#include "ui_interface.h"


class MainWindowPrivate;

class MainWindow : public QMainWindow, public Ui::MainWindow {

	Q_OBJECT
	Q_DECLARE_PRIVATE(MainWindow)

public:
	MainWindow();
	virtual ~MainWindow() = default;

	void init();
	void createActions();
	void createShortcuts();

	MainWindowPrivate* impl() const;

	void loadLayouts(const QByteArray& data);
	QByteArray saveLayouts() const;

private:
	QSharedPointer<MainWindowPrivate> d_ptr;
};




#endif // __MAINWINDOW_H__