// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QMenu>
#include <QTimer>
#include <QInputDialog>

#include "selectLayoutWidget.h"





Layout::Layout(const QString& name, SelectLayoutWidget& _widget):
	QAction(name),
    widget(_widget),
	remove(_widget.remove_menu->addAction(name))
{
    setCheckable(true);

	connect(remove, &QAction::triggered, [&] {
        if (isChecked()) checkNext();
        delete this;
	    _widget.enableFirst(false);
    });

    connect(this, &QAction::toggled, [this](bool checked) {
        if (checked) {
            widget.setText(text());

            if (!QAction::data().isNull())
                widget.mgr.restoreState(data());
        } else
            setData(widget.mgr.saveState());
    });

    widget.menu->insertAction(widget.separator, this);
    widget.layouts->addAction(this);
}


void Layout::checkNext() {

    QList<QAction*> actions = widget.layouts->actions();

    if (actions.size() > 1) {
        int index = actions.indexOf(this);
        Q_ASSERT(index != -1);

        actions[index ? index - 1 : 1]->setChecked(true);
    }
}


Layout::~Layout() {
	widget.remove_menu->removeAction(remove);
}

QByteArray Layout::data() const {
	return QAction::data().toByteArray();
}

void Layout::setEnabled(bool enabled) {
	remove->setEnabled(enabled);
}





SelectLayoutWidget::SelectLayoutWidget(ads::CDockManager& _mgr):
	menu(new QMenu(tr("Layouts"))),
    separator(menu->addSeparator()),
	layouts(new QActionGroup(this)),
	mgr(_mgr)
{
    layouts->setExclusive(true);
	setMenu(menu);
	setToolTip(tr("Layout"));
	show();

	save_action = menu->addAction(tr("Save Current Layout"));
	remove_menu = menu->addMenu(tr("Delete Layout"));

	connect(save_action, SIGNAL(triggered(bool)),
            this, SLOT(addLayout()));

	QTimer::singleShot(0, [this] {
        Layout* layout = new Layout(tr("default"), *this);
        layout->setChecked(true);
        layout->setEnabled(false);
    });
}


int SelectLayoutWidget::enableFirst(bool enable) {

	QList<QAction*> list = layouts->actions();
    int size = list.size();

	if (Q_UNLIKELY(size == 1))
		qobject_cast<Layout*>(list[0])->setEnabled(enable);

    return size;
}


QByteArray SelectLayoutWidget::serialize() const {

	Layout* current_layout = current();

    if (current_layout != Q_NULLPTR)
        current_layout->setData(mgr.saveState());

	QVector<QPair<QString, QByteArray>> items;
    QList<QAction*> actions = layouts->actions();
    items.reserve(actions.size());

	for (QAction* action : actions) {
        Layout* layout = qobject_cast<Layout*>(action);
		items.append({layout->text(), layout->data()});
	}

	QByteArray buffer;
	QDataStream out(&buffer, QIODevice::WriteOnly);
	out << items << actions.indexOf(current_layout);

	return buffer;
}


void SelectLayoutWidget::deserialize(const QByteArray& buffer) {

    for (QAction* action : layouts->actions())
        delete action;

	int id; QVector<QPair<QString, QByteArray>> items;
	QDataStream in(buffer);
	in >> items >> id;

    for (int i=0; i<items.size(); ++i) {
        const QPair<QString, QByteArray>& item = items[i];
        
        Layout* layout = new Layout(item.first, *this);
        layout->setData(item.second);

        if (Q_UNLIKELY(id == i)) layout->setChecked(true);
    }

    enableFirst(false);
}


void SelectLayoutWidget::addLayout() {

	QString new_name = getUnique(tr("Layout"));
	QString name;
	bool ok = false;

 	do {
        name = QInputDialog::getText(Q_NULLPTR, tr("New Layout"), "",
								QLineEdit::Normal, new_name, &ok);
		if (!ok) return;

	} while (name.isEmpty());

    int count = enableFirst(true);

	Layout* layout = new Layout(name, *this);
    layout->setChecked(true);

    if (count == 0) layout->setEnabled(false);
}


QString SelectLayoutWidget::getUnique(const QString& name) const {
	
	QString new_name = name;
	QStringList names;
    QList<QAction*> actions = layouts->actions();

	QString fmt("%1.%2");
	int i = 0;

	for (QAction* action : actions)
		names.append(action->text());

	while (names.contains(new_name))
		new_name = fmt.arg(name).arg(i++, 3, 10, QChar('0'));

	return new_name;
}


Layout* SelectLayoutWidget::current() const {
    return qobject_cast<Layout*>(layouts->checkedAction());
}

