// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <QFileSystemWatcher>
#include <QCommandLineParser>
#include <QApplication>
#include <QSettings>

#include "mainWindow.h"
#include "terrain.h"

class MainWindow;

class Application: public QApplication {

    Q_OBJECT

public:
    typedef std::function<void (Application*)> CreateCallback;
    
    Application(int& argc, char** argv);
    virtual ~Application() = default;

    static Application* globalInstance();
    static void onCreated(CreateCallback callback);
    static QString relativePath(const QString& name);

    QSettings settings;
    Terrain terrain;
    MainWindow window;
	QFileSystemWatcher watcher;

    typedef QMap<QString, Mesh> Objects;
    Objects objects;

    QString current_project;
    QCommandLineParser parser;

    void saveProjectData(const QString& path);

Q_SIGNALS:
    void projectOpened(const QString& path);

public Q_SLOTS:
	void reloadStyleSheet(const QString& path);

    void loadProject();
    void saveProject();
    void saveProjectAs();
    void saveCopy();

private:
    Q_DISABLE_COPY_MOVE(Application)

	void loadStyleSheet();
    void parseOptions();
    void init();

    typedef QVector<CreateCallback> Callbacks;
    static Callbacks callbacks;
};


#endif // __APPLICATION_H__