#version 430 core
#extension GL_EXT_geometry_shader4 : enable

#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 color;

// uniform layout(r16i) readonly uimage2D heightTex;

uniform sampler2D heightTex;
uniform sampler2D colorTex;

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 TRSMatrix;

out vec3 geomNormal;
out vec3 geomVertex;
out vec3 geomColor;

void main(void)
{
	float h = textureLod(heightTex, vertex.xy + 0.5, 0).r;
	// float h = imageLoad(heightTex, ivec2((vertex.xy + 0.5) * 1024)).x;


	vec3 vert 	  = vertex + vec3(0,0, h * 0.2);
	mat4 MVP      = ProjectionMatrix * ModelViewMatrix;
	gl_Position   = MVP * TRSMatrix * (vec4(vert, 1.0)); 
	geomNormal	  = (TRSMatrix * vec4(normalize(normal), 0.0f)).xyz;
	geomVertex 	  = vert;
	geomColor	  = color;
} 
#endif

#ifdef GEOMETRY_SHADER
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform vec2 WIN_SCALE;

in vec3 geomVertex[];
in vec3 geomNormal[];
in vec3 geomColor[];


out vec3 fragVertex;
out vec3 fragNormal;
out vec3 fragColor;
out vec3 dist;
out float ratio;

void main()
{	
	vec2 p0 = WIN_SCALE * gl_in[0].gl_Position.xy / gl_in[0].gl_Position.w;
	vec2 p1 = WIN_SCALE * gl_in[1].gl_Position.xy / gl_in[1].gl_Position.w;
	vec2 p2 = WIN_SCALE * gl_in[2].gl_Position.xy / gl_in[2].gl_Position.w;

	vec2 v0 = p2 - p1;
	vec2 v1 = p2 - p0;
	vec2 v2 = p1 - p0;

	float area = abs(v1.x * v2.y - v1.y * v2.x);

	// Triangle aspect ratio
	float ab = length(geomVertex[0] - geomVertex[1]);
	float bc = length(geomVertex[1] - geomVertex[2]);
	float ca = length(geomVertex[2] - geomVertex[0]);
	float s = 0.5f * (ab + bc + ca);
	float u = (s - ab) * (s - bc) * (s - ca);
	ratio = 8.0f * u / (ab * bc * ca);

	dist = vec3(area / length(v0), 0, 0);
	gl_Position = gl_in[0].gl_Position;
	fragVertex = geomVertex[0]; fragNormal = geomNormal[0]; fragColor = geomColor[0];
	EmitVertex();

	dist = vec3(0, area / length(v1), 0);
	gl_Position = gl_in[1].gl_Position;
	fragVertex = geomVertex[1]; fragNormal = geomNormal[1]; fragColor = geomColor[1];
	EmitVertex();

	dist = vec3(0, 0, area / length(v2));
	gl_Position = gl_in[2].gl_Position;
	fragVertex = geomVertex[2]; fragNormal = geomNormal[2]; fragColor = geomColor[2];
	EmitVertex();

	EndPrimitive();
}  

#endif

#ifdef FRAGMENT_SHADER
in vec3 fragVertex;
in vec3 fragNormal;
in vec3 fragColor;
in vec3 dist;
in float ratio;

uniform int material;
uniform int shading;
uniform int useWireframe;

out vec4 fragment;

// uniform layout(r16i) readonly uimage2D heightTex;
uniform sampler2D heightTex;
uniform sampler2D colorTex;

/*!
\brief Determine if a fragment should be discarded based on the shading mode.
This allows for controlling the render primitive (triangles, lines and points) in the fragment shader.
*/
bool ShouldDiscard()
{
	if (shading == 0) 	// Triangles
		return false;
	else				// Lines
		return (min(dist[0], min(dist[1], dist[2])) > 1);	
}


// Compute smooth diffuse color
// normal : Normal vector
// lighting : Lighting vector
float Diffuse(in vec3 normal, in vec3 lighting)
{
	// Modified diffuse lighting
	float d = 0.5 * (1.0 + dot(normal, lighting));
	return clamp(0.25 + (d * d), 0, 1);
}

vec4 GetBaseColor()
{
	// Normal
	if (material == 0)
	{
		return vec4(0.2 * (vec3(3.0) + 2.0 * fragNormal.xyz), 1.0);
	}
	// Color
	if (material == 1)
	{
		return vec4(fragColor.xyz * Diffuse(fragNormal, normalize(-vec3(0.2, 0.8, 0.0))), 1.0);
	}
}

float correctedHeigh(float z) {
	return floatBitsToUint(z);
}

vec3 computeNormal() {

	vec2 coord = fragVertex.xy + 0.5;
	vec2 texelSize = vec2(1.0) / textureSize(heightTex, 0).xy;

	float z = texture(heightTex, coord).x;
	float zx = texture(heightTex, coord + texelSize * vec2(1.0,0.0)).x;
	float zy = texture(heightTex, coord + texelSize * vec2(0.0,1.0)).x;

	float dx = (z - zx) / texelSize.x;
	float dy = (z - zy) / texelSize.y;

	return normalize(vec3(dx, dy, 1.0));
}

vec3 encodeSRGB(vec3 linearRGB) {

    vec3 a = 12.92 * linearRGB;
    vec3 b = 1.055 * pow(linearRGB, vec3(1.0 / 2.4)) - 0.055;
    vec3 c = step(vec3(0.0031308), linearRGB);

    return mix(a, b, c);
}


const float W = 11.2; // white scale

const mat3 ACESInputMat = mat3(
    0.59719, 0.35458, 0.04823,
    0.07600, 0.90834, 0.01566,
    0.02840, 0.13383, 0.83777
);

const mat3 ACESOutputMat = mat3(
    1.60475, -0.53108, -0.07367,
    -0.10208,  1.10813, -0.00605,
    -0.00327, -0.07276,  1.07602
);

const mat3 HighlightFixMat = mat3( // Prevents bright blues going to purple
	0.9404372683, 0.0183068787, 0.0778696104,
	0.0083786969, 0.8286599939, 0.1629613092,
	0.0005471261, 0.0008833746, 1.0003362486
);


vec3 RRTAndODTFit(vec3 v) {

    vec3 a = v * (v + 0.0245786) - 0.000090537;
    vec3 b = v * (0.983729 * v + 0.4329510) + 0.238081;
    return a / b;
}


vec3 ACESFitted(vec3 color) {
    color *= 5.0;
    color = RRTAndODTFit(color * HighlightFixMat * ACESInputMat);
    return color * ACESOutputMat;
}


void main()
{
	if (ShouldDiscard())
		discard;
	
	vec4 c = GetBaseColor();
	if (useWireframe == 1)
	{
		float d = min(dist[0], min(dist[1], dist[2]));
		float I = exp2(-7.0 * d * d);
		c 		= vec4((I * 0.85) * c.xyz + (1.0 - I) * c.xyz, 1.0);
	}
	c = texture(colorTex, fragVertex.xy + 0.5);
	vec3 lig = normalize(vec3( 0.3,0.5, 0.6));

	vec3 normal = computeNormal();
	float z = (texture(heightTex, fragVertex.xy + 0.5).x);

	vec3 diffuse = max(dot(normal, lig), 0.0) * vec3(1.0);
	diffuse += vec3(0.3, 0.35, 0.5) * 0.7;

	vec3 finalcolor = c.rgb * diffuse * 0.5;
	finalcolor = ACESFitted(finalcolor);

	fragment = vec4(finalcolor, 1.0);
	// fragment = vec4(vec3(z), 1.0);
}

#endif
