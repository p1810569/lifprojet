// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CURVESTAGE_H__
#define __CURVESTAGE_H__

#include "bezierCurve.h"
#include "terrainStage.h"


class CurveWidget: public QWidget {

public:
    CurveWidget(BezierCurve& curve, QWidget* parent=Q_NULLPTR);
    virtual ~CurveWidget() = default;

    void addHandle(const QPointF& pos);

protected:
    void curveChanged(BezierCurve::iterator it);
    void updateHandle(BezierCurve::iterator it);

    virtual void paintEvent(QPaintEvent *event) override;

    BezierCurve& curve;
    float tension;
};



class CurveStage: public TerrainStage {

    Q_OBJECT

public:
    CurveStage(Terrain& terrain, const QString name="curve");
    virtual ~CurveStage() = default;

    virtual void compute(QCLContext& context) override;
    virtual QWidget* embeddedWidget() override;

private:
    BezierCurve curve;
    CurveWidget* widget;
};

#endif // __CURVESTAGE_H__