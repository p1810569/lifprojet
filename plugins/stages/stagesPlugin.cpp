// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "application.h"
#include "imageStage.h"
#include "outputStage.h"
#include "stagesPlugin.h"
#include "constantStage.h"
#include "curveStage.h"


template<class T>
std::unique_ptr<QtNodes::NodeDataModel> makeStage() {

    Application* app = Application::globalInstance();
    return std::make_unique<T>(app->terrain);
}


void StagesPlugin::registerModel(QtNodes::DataModelRegistry& registery) {

    registery.registerModel(makeStage<ImageStage>, "Inputs");
    registery.registerModel(makeStage<OutputStage>, "Outputs");
    registery.registerModel(makeStage<CurveStage>, "Filters");

    registery.registerModel(makeStage<ScalarConstant>, "Constants");
    registery.registerModel(makeStage<Vector2Constant>, "Constants");
    registery.registerModel(makeStage<Vector3Constant>, "Constants");
    registery.registerModel(makeStage<Vector4Constant>, "Constants");
    // registery.registerModel(makeStage<ColorConstant>, "Constants");

    ScalarConstant::registerConverter(registery);
    Vector2Constant::registerConverter(registery);
    Vector3Constant::registerConverter(registery);
    Vector4Constant::registerConverter(registery);
    ColorConstant::registerConverter(registery);
}
