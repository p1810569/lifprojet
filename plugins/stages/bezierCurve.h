// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef __BEZIERCURVE_H__
#define __BEZIERCURVE_H__

#include <set>
#include <QList>
#include <QPointF>


class CurveHandle: public QPointF {

public:
    CurveHandle(float x=0.f);
	CurveHandle(const QPointF& pos);

    bool operator <(const CurveHandle& other) const {
        return x() < other.x();
    }

    mutable QPointF left_handle;
    mutable QPointF right_handle;
};


class BezierCurve: public std::set<CurveHandle> {

public:
    BezierCurve();
    ~BezierCurve() = default;


    double value(double x) const;

    iterator setCoord(iterator it, const QPointF& coord);

private:
    static double extrapolate(const QPointF& p0, const QPointF& p1, double x);

    static QList<double> findRoots(double a, double b, double c, double d);

    static double findTforX(double p0, double p1, double p2, double p3, double x);
};

#endif // __BEZIERCURVE_H__