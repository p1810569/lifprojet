// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QLabel>
#include <QVBoxLayout>
#include <QDoubleSpinBox>

#include "utils.h"

#include "constantStage.h"

#define REGISTER_CONSTANT_IMAGE_FMT(TYPE, ORDER) \
    template<> QCLImageFormat ConstantConverter<TYPE>::image_format = \
        { QCLImageFormat::ORDER, QCLImageFormat::Type_Float };


REGISTER_CONSTANT_IMAGE_FMT(float, Order_R)
REGISTER_CONSTANT_IMAGE_FMT(QVector2D, Order_RG)
REGISTER_CONSTANT_IMAGE_FMT(QVector3D, Order_RGB)
REGISTER_CONSTANT_IMAGE_FMT(QVector4D, Order_RGBA)
REGISTER_CONSTANT_IMAGE_FMT(Color, Order_RGBA)



QJsonValue serialize(const DoubleSpinBox* widget) {
    return widget->value();
}


void deserialize(DoubleSpinBox* widget, const QJsonValue& value) {
    widget->setValue(value.toDouble());
}








// void setupSplinBox(ConstantWidgetInterface* widget,
//                     DoubleSpinBox* container[], int n,
//                     QStringList labels=QStringList())
// {
//     for (int i=0; i<n; ++i) {
//         QWidget* line = new QWidget(widget);
//         QLabel* label = new QLabel(line);
//         container[i] = new DoubleSpinBox(line);
        
//         line->setLayout(new QHBoxLayout);

//         line->layout()->addWidget(label);
//         line->layout()->addWidget(container[i]);

//         widget->layout()->addWidget(line);

//         if (i < labels.size())
//             label->setText(labels[i]);

//         container[i]->setSingleStep(0.1);
//         container[i]->setMinimum(-INFINITY);
        
//         QObject::connect(container[i], SIGNAL(valueChanged(double)),
//                         widget, SLOT(valueChanged()));
//     }
// }







// void ConstantsWidget::typeChanged() {
    
//     QVariant value = selector->currentData();

//     switch (value.toInt()) {
//         case Scalar:
//         case Vector2:
//         case Vector3:
//         case Color:
//         default: break;
//     }
// }


    // setLayout(new QVBoxLayout);

    // connect(selector, SIGNAL(currentIndexChanged(const QString&)),
    //         this, SLOT(typeChanged()));

    // static const EnumInfo enum_info = getEnums(staticMetaObject, "Type");

    // for (const auto& enum_value: enum_info)
    //     selector->addItem(  enum_value.second,
    //                         enum_value.first);

