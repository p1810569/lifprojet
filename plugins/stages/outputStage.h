// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __OUTPUTSTAGE_H__
#define __OUTPUTSTAGE_H__

#include "terrainStage.h"


class OutputStage: public TerrainStage {

    Q_OBJECT

public:
    OutputStage(Terrain& terrain, const QString name="output");
    virtual ~OutputStage() = default;

    virtual void setInData( std::shared_ptr<QtNodes::NodeData> nodeData,
                            QtNodes::PortIndex port) override;

protected:
    virtual void setup(QCLContext& context) override;
    virtual void compute(QCLContext& context) override;
};


#endif // __OUTPUTSTAGE_H__