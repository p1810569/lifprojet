// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ressourceData.h"
#include "application.h"
#include "outputStage.h"

using namespace QtNodes;
using ImageRes = Ressource<QCLImage2D>;


OutputStage::OutputStage(Terrain& terrain, const QString name):
    TerrainStage(terrain, name)
{
    defineInput("height", "QCLImage2D");
    defineInput("color", "QCLImage2D");
}


void OutputStage::setInData(std::shared_ptr<NodeData> nodeData, PortIndex index) {

    TerrainStage::setInData(nodeData, index);

    Q_ASSERT(index < Terrain::COUNT);
    Terrain::BufferType port = (Terrain::BufferType)index;

    static const Qt::GlobalColor colors[] = {Qt::black, Qt::white};

    if (nodeData == Q_NULLPTR) {
        nodeData = RessourceFactory::getDefault("QCLImage2D");
        Q_EMIT terrain.clearBuffer(port, colors[port]);
    } else {
        ImageRes* res = static_cast<ImageRes*>(nodeData.get());
        Q_EMIT terrain.updateBuffer(port, res->value);
    }
}


void OutputStage::setup(QCLContext& context) {
    
}


void OutputStage::compute(QCLContext& context) {

}
