// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QPainterPath>
#include <QVector2D>

#include "curveStage.h"
#include "bezierCurve.h"









CurveWidget::CurveWidget(BezierCurve& _curve, QWidget* parent):
    QWidget(parent),
    curve(_curve),
	tension(1.0/3.0)
{
    
}


void CurveWidget::addHandle(const QPointF& pos) {

	curveChanged(curve.insert(pos).first);
}


void CurveWidget::curveChanged(BezierCurve::iterator it) {

	CurveHandle& handle = const_cast<CurveHandle&>(*it);

	if (it == curve.begin()) {
		
	} else if (it == std::prev(curve.end())) {

	}
}


void CurveWidget::updateHandle(BezierCurve::iterator it) {
	
	BezierCurve::iterator next = std::next(it);
	BezierCurve::iterator prev = std::prev(it);

	if (curve.size() == 2) {
		
	}

	QVector2D prev_vec(*std::prev(it) - *it);
	QVector2D next_vec(*std::next(it) - *it);

	float prev_length = prev_vec.length();
	float next_length = next_vec.length();

	QVector2D dir = (prev_length / next_length) * next_vec - prev_vec;
	dir.normalize();

	it->left_handle = *it - dir.toPointF() * tension * prev_length;
	it->right_handle = *it + dir.toPointF() * tension * next_length;
}



void CurveWidget::paintEvent(QPaintEvent* event) {

	QPainter painter(this);

	painter.fillRect(rect(), QColor(30,30,30));

	QPen pointPen;
	pointPen.setWidth(4);
	pointPen.setColor(Qt::red);

	painter.setPen(pointPen);

	for (const CurveHandle& handle: curve)
		painter.drawPoint(handle);

    QPainterPath path;
    using HandleIt = BezierCurve::const_iterator;

    HandleIt it = curve.cbegin();
	
	if (it->x() > 0.f) {
		path.moveTo(0.f, curve.value(0.f));
		path.lineTo(*it);
	}

	while (it != std::prev(curve.end())) {
        HandleIt next = std::next(it);
        path.cubicTo(it->right_handle, next->left_handle, *next);
		it = next;
	}

	if (it->x() < 1.f)
		path.lineTo(1.f, curve.value(1.f));
}


// int SplineDisplayerWidget::checkSelected(aaAaa::Vector2 point){
	// if(!m_spline_data){
	// 	return -1;
	// 	emit selectValuesChanged(0, 0);
	// }
	// aaAaa::aaSpline &m_spline_data = *(this->m_spline_data);

	// float factor = (m_CameraPos.z - Z_VALUE) * 0.1f * 0.5f;
	// for(size_t i=0; i<m_spline_data.knots.size(); ++i){
	// 	if(abs(point.t - m_spline_data.knots[i].t) < factor && abs(point.y - m_spline_data.knots[i].y) < factor ){
	// 		emit selectValuesChanged(m_spline_data.knots[i].t, m_spline_data.knots[i].y);
	// 		return i;
	// 	}
	// }
	
	// emit selectValuesChanged(0, 0);
// 	return -1;
// }


// void SplineDisplayerWidget::mousePressEvent(QMouseEvent *event) {

// 	//if(event->buttons() & Qt::MidButton)
// 	lastPos = event->pos();
	
// //	if(ctrlSelected == -1){
// 	if(event->button() == Qt::LeftButton || event->button() == Qt::RightButton){
// 		aaAaa::Vector2 glpoint = screen2gl(event->x(), event->y());

// 		ctrlSelected = checkSelected(glpoint);

// 		emit selectValuesChanged(glpoint.t, glpoint.y);
// //			std::cout << "glpoint: (" << glpoint.t << ", " << glpoint.y << "); ctrlSelected: " << ctrlSelected << "\n";
// 	}

// 	updateGL();
// }

// void SplineDisplayerWidget::mouseReleaseEvent(QMouseEvent *event) {
	// if(!m_spline_data)
	// 	return ;
	// aaAaa::aaSpline &m_spline_data = *(this->m_spline_data);

	// if(event->button() == Qt::RightButton){
	// 	aaAaa::aaSpline::KnotsList &ctrlpoints = m_spline_data.knots;
	// 	if (ctrlSelected >= 0){
	// 		if(ctrlSelected != 0 && ctrlSelected != ctrlpoints.size()-1){
	// 			ctrlpoints.erase(ctrlpoints.begin() + ctrlSelected);
	// 		}

	// 		aaAaa::Vector2 glpoint = screen2gl(event->x(), event->y());
	// 		ctrlSelected = checkSelected(glpoint);

	// 	}else{
	// 		aaAaa::Vector2 p = screen2gl(event->x(), event->y());
	// 		if(p.t > ctrlpoints[0].t &&
	// 			p.t < ctrlpoints[ctrlpoints.size()-1].t){

	// 			aaAaa::aaSpline::KnotsList::iterator it = ctrlpoints.begin();
	// 			++it;
	// 			for(; it!=ctrlpoints.end(); ++it){
	// 				if(p.t < (*it).t)
	// 					break;
	// 			}

	// 			ctrlpoints.insert(it, p);

	// 			aaAaa::Vector2 glpoint = screen2gl(event->x(), event->y());
	// 			ctrlSelected = checkSelected(glpoint);
	// 		}
	// 	}

	// 	updateGL();
	// }
	// //ctrlSelected = -1;
// }



// void SplineDisplayerWidget::mouseMoveEvent(QMouseEvent *event) {
	
	// int dx = event->x() - lastPos.x();
	// int dy = event->y() - lastPos.y();

	

	// if (ctrlSelected >= 0 &&
	// 	event->buttons() & Qt::LeftButton && m_spline_data){
			
	// 		aaAaa::aaSpline &m_spline_data = *(this->m_spline_data);

	// 		aaAaa::aaSpline::KnotsList &ctrlpoints = m_spline_data.knots;
	// 		aaAaa::Vector2 npos = screen2gl(event->x(), event->y());
	// 		ctrlpoints[ctrlSelected].y = npos.y;
	// 		ctrlpoints[ctrlSelected].t = npos.t;
			
	// 		if(ctrlSelected < ctrlpoints.size() - 1){
	// 			if(ctrlpoints[ctrlSelected].t >= ctrlpoints[ctrlSelected+1].t)
	// 				ctrlpoints[ctrlSelected].t = ctrlpoints[ctrlSelected+1].t - 1;
	// 		}
	// 		if(ctrlSelected > 0){
	// 			if(ctrlpoints[ctrlSelected].t <= ctrlpoints[ctrlSelected-1].t)
	// 				ctrlpoints[ctrlSelected].t = ctrlpoints[ctrlSelected-1].t + 1;
	// 		}

	// 		if(m_spline_data.bLimited){
	// 			if(ctrlpoints[ctrlSelected].t < m_spline_data.limit_left){
	// 				ctrlpoints[ctrlSelected].t = m_spline_data.limit_left;
	// 			}else if(ctrlpoints[ctrlSelected].t > m_spline_data.limit_right){
	// 				ctrlpoints[ctrlSelected].t = m_spline_data.limit_right;
	// 			}

	// 			if(ctrlpoints[ctrlSelected].y < m_spline_data.limit_bottom){
	// 				ctrlpoints[ctrlSelected].y = m_spline_data.limit_bottom;
	// 			}else if(ctrlpoints[ctrlSelected].y > m_spline_data.limit_top){
	// 				ctrlpoints[ctrlSelected].y = m_spline_data.limit_top;
	// 			}
	// 		}

	// 		emit selectValuesChanged(ctrlpoints[ctrlSelected].t, ctrlpoints[ctrlSelected].y);

	// 		m_bIfModify = true;
	// }else if(event->buttons() & Qt::MidButton){
	// 	m_CameraPos.x = (m_CameraPos.x - dx*0.3f);
	// 	m_CameraPos.y = (m_CameraPos.y + dy*0.3f);	
	// }
	// lastPos = event->pos();
	// updateGL();
// }


// void SplineDisplayerWidget::setCurrentSelected(int index){
// 	if(!m_spline_data)
// 		return ;
// 	if(index >= 0 && index < m_spline_data->knots.size()){
// 		ctrlSelected = index;
// 		emit selectValuesChanged(m_spline_data->knots[ctrlSelected].t, m_spline_data->knots[ctrlSelected].y);
// 	}
// 	// updateGL();
// }








void CurveStage::compute(QCLContext& context) {
    
}

QWidget* CurveStage::embeddedWidget() {
    return widget;
}


CurveStage::CurveStage(Terrain& terrain, const QString name):
    TerrainStage(terrain, name),
    widget(new CurveWidget(curve))
{
}
