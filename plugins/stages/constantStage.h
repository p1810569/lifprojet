// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONSTANTSTAGE_H__
#define __CONSTANTSTAGE_H__

#include <nodes/DataModelRegistry>

#include "ressourceData.h"
#include "color.h"
#include "terrainStage.h"
#include "widgets.h"


template<class T>
struct ConstantWidget { using type = Null; };


#define REGISTER_CONSTANT_TYPE(TYPE, WIDGET) \
    template<> struct ConstantWidget<TYPE> { using type = WIDGET; }; \



REGISTER_CONSTANT_TYPE(float, DoubleSpinBox)
REGISTER_CONSTANT_TYPE(QVector2D, SpinBoxVector2D)
REGISTER_CONSTANT_TYPE(QVector3D, SpinBoxVector3D)
REGISTER_CONSTANT_TYPE(QVector4D, SpinBoxVector4D)
// REGISTER_CONSTANT(Color, SpinBoxVector4D, Order_RGB)


template<class T>
class ConstantStage: public TerrainStage {

public:
    ConstantStage(Terrain& terrain);
    virtual ~ConstantStage() = default;

    virtual void compute(QCLContext& context) override;
    virtual void setup(QCLContext& context) override;

    virtual QJsonObject save() const override;
    virtual void restore(const QJsonObject& data) override;

    virtual QWidget* embeddedWidget() override;
    
    static void registerConverter(QtNodes::DataModelRegistry& factory);
    
    void valueChanged();

    typedef typename ConstantWidget<T>::type WidgetType;
    typedef T DataType;

protected:
    WidgetType* widget;
};


template<class T>
class ConstantConverter: public StageConverter {

public:
    ConstantConverter(Terrain& terrain);
    ConstantConverter(const ConstantConverter<T>& copy);

    virtual ~ConstantConverter() = default;

    QtNodes::SharedNodeData operator() (QtNodes::SharedNodeData input);

protected:
    virtual void setup(QCLContext& context) override;

    static QCLImageFormat image_format;

    typedef Ressource<QCLImage2D> ImageRes;
    std::shared_ptr<ImageRes> ressource;
};


typedef ConstantStage<float> ScalarConstant;
typedef ConstantStage<QVector2D> Vector2Constant;
typedef ConstantStage<QVector3D> Vector3Constant;
typedef ConstantStage<QVector4D> Vector4Constant;
typedef ConstantStage<Color> ColorConstant;

#include "constantStage.T"

#endif // __CONSTANTSTAGE_H__












// CONSTANT_WIDGET_DATA(float) {
//     DoubleSpinBox* spinbox;
// };

// CONSTANT_WIDGET_DATA(QVector2D) {
//     std::array<DoubleSpinBox*, 2> spinbox;
// };


// template<uint8_t T> 
// struct ConstantWidgetData<T> {

// };


// class ConstantWidgetInterface: public EmbeddedWidget {

//     Q_OBJECT

// public:
//     ConstantWidgetInterface() = default;
//     virtual ~ConstantWidgetInterface() = default;

// protected Q_SLOTS:
//     virtual void valueChanged() = 0;
// };


// template<class T>
// class ConstantWidget: public ConstantWidgetInterface {

// public:
//     ConstantWidget(ConstantStage<T>& stage);
//     virtual ~ConstantWidget() = default;

//     QJsonValue serialize() const;
//     void deserialize(const QJsonValue& value);

// private:
//     ConstantWidgetData<T> data;

//     void setup();
//     virtual void valueChanged() override;

//     ConstantStage<T>& stage;
// };