// // Copyright 2021 matthieu
// // 
// // Licensed under the Apache License, Version 2.0 (the "License");
// // you may not use this file except in compliance with the License.
// // You may obtain a copy of the License at
// // 
// //     http://www.apache.org/licenses/LICENSE-2.0
// // 
// // Unless required by applicable law or agreed to in writing, software
// // distributed under the License is distributed on an "AS IS" BASIS,
// // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// // See the License for the specific language governing permissions and
// // limitations under the License.

#include <math.h>
#include <QVector2D>

#include "utils.h"
#include "bezierCurve.h"


#define TAU 2.0 * M_PI



inline bool almostNull(double x) {
    return abs(x) < 1e-8;
}


CurveHandle::CurveHandle(const QPointF& coord):
	QPointF(coord)
{
	
}


CurveHandle::CurveHandle(float x):
    QPointF(x, 0.f)
{
    
}

BezierCurve::BezierCurve():
    set({QPointF(0,0), QPointF(1,1)})
{

}


double BezierCurve::value(double x) const {

    Q_ASSERT(size() >= 2);

    const_iterator it = upper_bound(x);

    if (it == begin())
        return extrapolate(*it, it->right_handle, x);
    else if (it == end()) {
        const_iterator last = std::prev(it);
        return extrapolate(*last, last->left_handle, x);
    }
    
    const_iterator other = std::prev(it);

    double t = findTforX(other->x(), other->right_handle.x(),
                        it->left_handle.x(), it->x(), x);

    double A = other->y();
    double B = other->right_handle.y();
    double C = it->left_handle.y();
    double D = it->y();

    double t2 = 1.0 - t;
    return t2*t2*t2*A + 3.0*t2*t2*t*B + 3.0*t2*t*t*C + t*t*t*D;
}


BezierCurve::iterator BezierCurve::setCoord(iterator it, const QPointF& coord) {

    CurveHandle handle(coord);
    handle.left_handle = coord + it->left_handle;
    handle.right_handle = coord + it->right_handle;

    erase(it);
    return insert(handle).first;
}


double BezierCurve::extrapolate(const QPointF& p0, const QPointF& p1, double x) {

    double denum = (p1.x() - p0.x());

    if (Q_UNLIKELY(denum == 0.f))
        return p0.x();

    double a = (p1.y() - p0.y()) / denum;
    double b = p0.y() - a * p0.x();

    return fma(a, x, b);
}


QList<double> BezierCurve::findRoots(double a, double b, double c, double d) {

    // Fun fact: any Bezier curve may (accidentally or on purpose)
    // perfectly model any lower order curve, so we want to test 
    // for that: lower order curves are much easier to root-find.
    if (almostNull(a)) {
        // this is not a cubic curve.
        if (almostNull(b)) {
            // in fact, this is not a quadratic curve either.
            if (almostNull(c))
                return {}; // in fact in fact, there are no solutions.
        // linear solution:
            return {-d / c};
        }
        // quadratic solution:
        double q = sqrt(c * c - 4.0 * b * d);
        double b2 = 2.0 * b;

        return {(q - c) / b2, (-c - q) / b2};
    }
    // At this point, we know we need a cubic solution,
    // and the above a/b/c/d values were technically
    // a pre-optimized set because a might be zero and
    // that would cause the following divisions to error.

    b /= a;
    c /= a;
    d /= a;

    double b3 = b / 3.0;
    double p = (3.0 * c - b*b) / 3.0; 
    double p3 = p / 3.0; 
    double q = (2.0 * b*b*b - 9 * b * c + 27.0 * d) / 27.0; 
    double q2 = q / 2; 
    double discriminant = q2*q2 + p3*p3*p3; 
    double u1, v1;

    // case 1: three real roots, but finding them involves complex
    // maths. Since we don't have a complex data type, we use trig
    // instead, because complex numbers have nice geometric properties.
    if (discriminant < 0) {
        double mp3 = -p / 3.0;
        double r = sqrt(mp3*mp3*mp3); 
        double t = -q / (2.0 * r);
        double cosphi = clamp(t, -1.0, 1.0), 
        phi = acos(cosphi), 
        crtr = cbrt(r), 
        t1 = 2.0 * crtr;

        return {t1 * cos(phi / 3) - b3,
                t1 * cos((phi + TAU) / 3.0) - b3,
                t1 * cos((phi + 2.0 * TAU) / 3.0) - b3};
    }

    // case 2: three real roots, but two form a "double root",
    // and so will have the same resultant value. We only need
    // to return two values in this case.
    else if (discriminant == 0.0) {
        u1 = std::copysign(cbrt(-q2), -q2);
        return {2.0 * u1 - b3, -u1 - b3};
    }

    // case 3: one real root, 2 complex roots. We don't care about
    // complex results so we just ignore those and directly compute
    // that single real root.
    double sd = sqrt(discriminant);
    u1 = cbrt(-q2 + sd);
    v1 = cbrt(q2 + sd);
    return {u1 - v1 - b3};
}


double BezierCurve::findTforX(double p0, double p1, double p2, double p3, double x) {

    // a b c d are the bernstein coefficients
    // (-a + 3b- 3c + d)t³ + (3a - 6b + 3c)t² + (-3a + 3b)t + (a-x) = 0

    double pa3 = 3.0 * p0;
    double pb3 = 3.0 * p1;
    double pc3 = 3.0 * p2;
    double a = -p0  + pb3 - pc3 + p3;
    double b =  pa3 - 2.0 * pb3 + pc3;
    double c = -pa3 + pb3;
    double d =  p0 - x;

    for (double t : findRoots(a, b, c, d)) {
        if (t >= 0.0 && t <= 1.0)
            return t;
    }
    return -1.0;
}













































    // QList<double> cubicRoots(double a, double b, double c, double d) {

    //     if (almostNull(a)) { // Quadratic case, ax^2+bx+c=0
    //         a = b; b = c; c = d;
    //         if (almostNull(a)) { // Linear case, ax+b=0
    //             a = b; b = c;
    //             if (almostNull(a)) // Degenerate case
    //                 return {};
    //             return {-b / a};
    //         }

    //         double D = b * b - 4 * a * c;
    //         if (almostNull(D))
    //             return {-b / (2.0 * a)};
    //         else if (D > 0) {
    //             double Dsqrt = sqrt(D);
    //             return {(-b + Dsqrt) / (2.0 * a), (-b - Dsqrt) / (2.0 * a)};
    //         }
    //         return {};
    //     }

    //     // Convert to depressed cubic t^3+pt+q = 0 (subst x = t - b/3a)
    //     double p = (3.0*a*c - b*b) / (3.0*a*a);
    //     double q = (2.0*b*b*b - 9.0*a*b*c + 27.0*a*a*d) / (27.0*a*a*a);
    //     QList<double> roots;

    //     if (almostNull(p)) // p = 0 -> t^3 = -q -> t = -q^1/3
    //         roots = { cbrt(-q) };
    //     else if (almostNull(q)) { // q = 0 -> t^3 + pt = 0 -> t(t^2+p)=0
    //         roots = {0.0};
    //         if (p < 0)
    //             roots.append({0.0, sqrt(-p), -sqrt(-p)});
    //     } else {
    //         double D = q * q / 4.0 + p * p * p / 27.0;
    //         if (almostNull(D))       // D = 0 -> two roots
    //             roots = {-1.5 * q / p, 3.0 * q / p};
    //         else if (D > 0) {             // Only one real root
    //             double u = cbrt(-q / 2.0 - sqrt(D));
    //             roots = {u - p / (3.0 * u)};
    //         } else {                        // D < 0, three roots, but needs to use complex numbers/trigonometric solution
    //             double u = 2.0 * sqrt(-p / 3.0);
    //             double t = acos(3 * q / p / u) / 3.0;  // D < 0 implies p < 0 and acos argument in [-1..1]
    //             double k = 2.0 * M_PI / 3.0;
    //             roots = {u * cos(t), u * cos(t-k), u * cos(t - 2.0 * k)};
    //         }
    //     }

    //     // Convert back from depressed cubic
    //     for (var i = 0; i < roots.length; i++)
    //         roots[i] -= b/(3*a);

    //     return roots;
    // }

