// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <qclcontext.h>
#include <QTimer>
#include <QPushButton>
#include <QFileDialog>
#include <QStandardPaths>
#include <QImageReader>
#include <QLabel>
#include <QVBoxLayout>

#include "ressourceData.h"
#include "terrain.h"
#include "imageStage.h"


typedef Ressource<QCLImage2D> ImageRes;

inline QCLImage2D& getImage(TerrainStage::PortEntry& entry) {
    
    ImageRes* ptr = static_cast<ImageRes*>(entry.data.get());
    Q_ASSERT(ptr != Q_NULLPTR);

    return ptr->value;
}


ImageStage::ImageStage(Terrain& terrain, const QString name):
    TerrainStage(terrain, name),
    embedded_widget(new EmbeddedWidget)
{
    embedded_widget->setLayout(new QVBoxLayout);

    QPushButton* button = new QPushButton(tr("open"), embedded_widget);
    QLabel* label = new QLabel(embedded_widget);
    label->setMaximumSize(QSize(128,128));

    embedded_widget->layout()->addWidget(button);
    embedded_widget->layout()->addWidget(label);

    connect(button, SIGNAL(clicked()), this, SLOT(open()));

    defineOutput("image", "QCLImage2D").data =
        std::make_shared<ImageRes>();
}


QWidget* ImageStage::embeddedWidget() {
    return embedded_widget;
}


void ImageStage::compute(QCLContext& context) {

}


void ImageStage::setup(QCLContext& context) {

    QCLImage2D& image = getImage(outputs[0]);

    if (QFileInfo::exists(path))
        loadImage(terrain.context, path);
    else
        image = context.createImage2DDevice(default_format,
            terrain.size(), QCLImage2D::ReadOnly);
}


void ImageStage::loadImage(QCLContext& context, const QString& path) {
    
    QCLImage2D& data = getImage(outputs[0]);

    if (image.load(path)) {
        this->path = path;

        data = context.createImage2DCopy(image, QCLMemoryObject::ReadOnly);
        QLabel* label = embedded_widget->findChild<QLabel*>();
        label->setPixmap(QPixmap::fromImage(image));

        Q_EMIT dataUpdated(0);
    } else
        qCritical() << "failed to load image:" << path;
}


QJsonObject ImageStage::save() const {
    
    QJsonObject object = TerrainStage::save();
    object["path"] = path;

    return object;
}


void ImageStage::restore(const QJsonObject& data) {
    
    path = data["path"].toString();
}


void initFileDialog(QFileDialog& dialog) {

    static bool firstDialog = true;

    if (Q_UNLIKELY(firstDialog)) {
        firstDialog = false;

        const QStringList locations = QStandardPaths::standardLocations(
            QStandardPaths::PicturesLocation);
        
        dialog.setDirectory(locations.isEmpty() ? 
            QDir::currentPath() : locations.last());
    }

    QStringList mime_type_filters;
    const QByteArrayList supported_mime = QImageReader::supportedMimeTypes();

    for (const QByteArray& mime_name : supported_mime)
        mime_type_filters.append(mime_name);

    mime_type_filters.sort();
    dialog.setMimeTypeFilters(mime_type_filters);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);  
}


void ImageStage::open() {

    QFileDialog dialog(embedded_widget, tr("Open Image"));
    initFileDialog(dialog);

    if (dialog.exec() == QDialog::Accepted) {

        QStringList files = dialog.selectedFiles();
        loadImage(terrain.context, files.constFirst());
    }
}
