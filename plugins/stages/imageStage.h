// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __IMAGESTAGE_H__
#define __IMAGESTAGE_H__

#include "ressourceData.h"
#include "terrainStage.h"

class ImageStage: public TerrainStage {

    Q_OBJECT

public:
    ImageStage(Terrain& terrain, const QString name="image");
    virtual ~ImageStage() = default;

    virtual QWidget* embeddedWidget() override;

    void loadImage(QCLContext& context, const QString& path);

    virtual QJsonObject save() const override;
    virtual void restore(const QJsonObject& data) override;

protected:
    virtual void setup(QCLContext& context) override;
    virtual void compute(QCLContext& context) override;

    QString path;
    QImage image;
    QWidget* embedded_widget;

private Q_SLOTS:
    void open();
};


#endif // __IMAGESTAGE_H__