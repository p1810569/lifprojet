// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <nodes/FlowView>
#include <nodes/FlowScene>
#include <nodes/Node>
#include <QVBoxLayout>
#include <QMenuBar>
#include <QEvent>

#include "widgets.h"
#include "terrainStage.h"
#include "application.h"
#include "pluginManager.h"
#include "terrainStagePlugin.h"
#include "nodeEditor.h"

using namespace std;
using namespace QtNodes;


class NodeEditorView: public FlowView {

public:
    NodeEditorView(QtNodes::FlowScene* scene=Q_NULLPTR);
    virtual ~NodeEditorView() = default;
};


NodeEditorView::NodeEditorView(QtNodes::FlowScene* scene):
    QtNodes::FlowView(scene)
{
    setRenderHint(QPainter::Antialiasing); 
    setRenderHint(QPainter::SmoothPixmapTransform);
    setWindowFlag(Qt::BypassGraphicsProxyWidget);

    // setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    // setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
}



NodeEditor::NodeEditor(QWidget* parent):
    QWidget(parent),
    registery(new QtNodes::DataModelRegistry)
{

    Application* app = Application::globalInstance();

    QtNodes::FlowScene* scene = &app->terrain;
    scene->setRegistry(registery);


    QMenuBar* menu_bar = new QMenuBar(this);
    add_menu = menu_bar->addMenu(tr("add"));
    menu_bar->setNativeMenuBar(false);

    QVBoxLayout* layout = new QVBoxLayout(this);

    layout->addWidget(menu_bar);
    layout->addWidget(new NodeEditorView(scene));
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(0);

    installEventFilter(this);
}


void NodeEditor::loadTerrainStages() {

    registerDataModels();

    add_menu->clear();

    for (const auto& it: registery->registeredModelCreators()) {
        const QString& name = it.first;

        QAction* action = add_menu->addAction(name);

        connect(action, SIGNAL(triggered(bool)),
                this, SLOT(createNode()));
    }
}


void NodeEditor::registerDataModels() {

    PluginManager* mgr = PluginManager::globalInstance();

    QList<QSharedPointer<TerrainStagePlugin>> plugins = 
        mgr->getPlugins<TerrainStagePlugin>();

    for (QSharedPointer<TerrainStagePlugin>& plugin: plugins)
        plugin->registerModel(*registery);
}


bool NodeEditor::eventFilter(QObject* watched, QEvent* event) {

    if (event->type() == QEvent::DynamicPropertyChange) {
        QDynamicPropertyChangeEvent* prop = 
            static_cast<QDynamicPropertyChangeEvent*>(event);

        if (prop->propertyName() == "dockWidgetContent") {
            loadTerrainStages();
            removeEventFilter(this);
        }
    }
	return QObject::eventFilter(watched, event);
}


void NodeEditor::createNode() {
    
    QAction* action = qobject_cast<QAction*>(sender());

    Application* app = Application::globalInstance();
    auto model = registery->create(action->text());

    if (model != Q_NULLPTR)
        app->terrain.createNode(qMove(model));
}



NodeEditorPlugin::NodeEditorPlugin():
    editor_widget(new NodeEditor)
{

}


QString NodeEditorPlugin::label() {
    return "Node Editor";
}


QWidget* NodeEditorPlugin::widget() {
    return editor_widget;
}

