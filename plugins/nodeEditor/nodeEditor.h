// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __NODEEDITOR_H__
#define __NODEEDITOR_H__

#include <nodes/DataModelRegistry>
#include <nodes/FlowScene>

#include "editorPlugin.h"

class QMenu;


class NodeEditor: public QWidget {

    Q_OBJECT

public:
    NodeEditor(QWidget* parent=Q_NULLPTR);
    virtual ~NodeEditor() = default;

    void loadTerrainStages();

private:
    typedef std::shared_ptr<QtNodes::DataModelRegistry> Registery;

    Registery registery;
    QMenu* add_menu;

    void registerDataModels();
    virtual bool eventFilter(QObject* watched, QEvent* event) override;

private Q_SLOTS:
    void createNode();
};


class NodeEditorPlugin: public EditorPlugin {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.alluvion.editor")
    Q_INTERFACES(EditorPlugin)

public:
    NodeEditorPlugin();
    ~NodeEditorPlugin() = default;

    virtual QString label() override;
    virtual QWidget* widget() override;

private:
    QWidget* editor_widget;
};

#endif // __NODEEDITOR_H__