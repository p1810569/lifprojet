// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// theses lines are mandatory in order to auto-register these classes
#include "subStage.h"
#include "subStageGroup.h"

#include "declarativeStage.h"


using DataPtr = DeclarativeStage::DataPtr;
using NodeRef = SubStageNode::NodeRef;
using Constructors = QMap<QString, std::function<SubStageNode::Construct>>;


REGISTER_SUBSTAGE_NODE(SubStageGroup)
REGISTER_SUBSTAGE_NODE(SubStage)


Constructors& factory() {

    static Constructors constructors;
    return constructors;
}


NodeRef SubStageNode::createNode(const QJsonObject& data) {

    QString id = data["type"].toString();
    Constructors::const_iterator it = factory().find(qMove(id));

    if (Q_LIKELY(it != factory().end())) {
        const auto& construct = it.value();
        return construct(data);
    } else
        qWarning() << "Attempt to create unregestered node type :" << id;

    return Q_NULLPTR;
}



SubStageNode::SubStageNode(const QJsonObject& _data):
    data(_data)
{

}

int SubStageNode::current_iter() const {
    return iter;
}

int SubStageNode::num_iter() const {
    return repeat->value;
}


void SubStageNode::exec(SubStageNode* parent) {

    if (repeat == Q_NULLPTR)
        return;

    for (iter=0; iter<repeat->value; ++iter)
        run(parent);
}

void SubStageNode::run(SubStageNode* parent) {
    
}


void SubStageNode::setup(DeclarativeStage* stage) {

    if (data.contains("repeatRessourceID")) {
        QString rep_id = data["repeatRessourceID"].toString();
        DataPtr res = stage->getRessource(rep_id);

        Q_ASSERT(res != Q_NULLPTR && res->isOfType<cl_int>());
        repeat = std::dynamic_pointer_cast<Ressource<cl_int>>(res);

    } else {
        int num = data["repeat"].toInt(1);
        repeat = std::make_shared<Ressource<cl_int>>(num);
    }
}


