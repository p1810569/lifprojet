// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SUBSTAGENODE_H__
#define __SUBSTAGENODE_H__


#include <qclkernel.h>

#include "terrainStage.h"
#include "ressourceData.h"



#define REGISTER_SUBSTAGE_NODE(Type) \
    INITIALIZER(type##substage_node) { SubStageNode::registerNode<Type>(#Type); }


class SubStageNode: public QObject {

    Q_OBJECT

public:
    SubStageNode(const QJsonObject& data);
    virtual ~SubStageNode() = default;

    void exec(SubStageNode* parent);

    virtual void run(SubStageNode* parent) = 0;
    virtual void setup(DeclarativeStage* stage) = 0;

    typedef QSharedPointer<SubStageNode> NodeRef;
    typedef NodeRef Construct(const QJsonObject&);

    template<class T>
    static void registerNode(const QString& id);
    static NodeRef createNode(const QJsonObject& data);

    int current_iter() const;
    int num_iter() const;

protected:
    QJsonObject data;

private:
    std::shared_ptr<Ressource<cl_int>> repeat;
    int iter;
};


#include "subStageNode.T"


#endif // __SUBSTAGENODE_H__