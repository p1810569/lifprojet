// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <qclcontext.h>

#include <QStyledItemDelegate>
#include <QListView>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <nodes/Node>
#include <QVBoxLayout>
#include <QTimer>

#include "property.h"
#include "pluginManager.h"
#include "utils.h"
#include "declarativeStagePlugin.h"
#include "terrain.h"
#include "ressourceData.h"
#include "kernelManager.h"
#include "declarativeStage.h"

using namespace std;

/**
 * @file        declarativeStage.cpp
 * @brief       Contient la définition de la classe DeclarativeStage
 * @details     La classe \c DeclarativeStage permet de montrer l'utilisation des \em tags \b Doxygen
 * @author      Matthieu Jacquemet <Matthieu.jacquemet@univ-lyon1.fr>
 * @date        2021
 */

DeclarativeStage::PropConstructors DeclarativeStage::prop_constructors;
QStringList DeclarativeStage::args;


template<class T>
using RessourceHandler = QOverload<Ressource<T>*, Terrain&, QJsonObject&>;
using Handler = void (*)(TerrainStage::PortData*, Terrain&, QJsonObject&);

using DataPtr = DeclarativeStage::DataPtr;



#define HANDLE_RESSOURCE(TYPE) \
    { #TYPE, (Handler)RessourceHandler<TYPE>::of(&setupRessource) }


INITIALIZER(args) {

    Application::onCreated([](Application* app) {

        QVariant var = app->settings.value("solver/arguments");
        DeclarativeStage::addArgs(var.toStringList());
    });
}

/**
 * @brief       Désérialisation du format de l'image
 * @param       data Format de l'image au format Json de la forme:
 *              {
 *                  "channelFormat": String,
 *                  "channelType": String
 *              }
 */ 
QCLImageFormat getImageFormat(const QJsonObject& data) {

    QString order_id = data["channelFormat"].toString();
    QString type_id = data["channelType"].toString();

    typedef QMap<QString, QCLImageFormat::ChannelOrder> OrderMapping;
    typedef QMap<QString, QCLImageFormat::ChannelType> TypeMapping;

    static const OrderMapping orders {
        {"r", QCLImageFormat::Order_R},
        {"a", QCLImageFormat::Order_A},
        {"rg", QCLImageFormat::Order_RG},
        {"ra", QCLImageFormat::Order_RA},
        {"rgb", QCLImageFormat::Order_RGB},
        {"rgba", QCLImageFormat::Order_RGBA},
        {"bgra", QCLImageFormat::Order_BGRA},
        {"argb", QCLImageFormat::Order_ARGB},
        {"intensity", QCLImageFormat::Order_Intensity},
        {"luminance", QCLImageFormat::Order_Luminence},
        {"rx", QCLImageFormat::Order_Rx},
        {"rgx", QCLImageFormat::Order_RGx},
        {"rgbx", QCLImageFormat::Order_RGBx}
    };

    static const TypeMapping types {
        {"normalized_int8", QCLImageFormat::Type_Normalized_Int8},
        {"normalized_int16", QCLImageFormat::Type_Normalized_Int16},
        {"normalized_uint8", QCLImageFormat::Type_Normalized_UInt8},
        {"normalized_uint16", QCLImageFormat::Type_Normalized_UInt16},
        {"normalized_565", QCLImageFormat::Type_Normalized_565},
        {"normalized_555", QCLImageFormat::Type_Normalized_555},
        {"normalized_101010", QCLImageFormat::Type_Normalized_101010},
        {"unnormalized_int8", QCLImageFormat::Type_Unnormalized_Int8},
        {"unnormalized_int16", QCLImageFormat::Type_Unnormalized_Int16},
        {"unnormalized_int32", QCLImageFormat::Type_Unnormalized_Int32},
        {"unnormalized_uint8", QCLImageFormat::Type_Unnormalized_UInt8},
        {"unnormalized_uint16", QCLImageFormat::Type_Unnormalized_UInt16},
        {"unnormalized_uint32", QCLImageFormat::Type_Unnormalized_UInt32},
        {"half_float", QCLImageFormat::Type_Half_Float},
        {"float", QCLImageFormat::Type_Float}
    };

    OrderMapping::const_iterator oit = orders.find(order_id);
    TypeMapping::const_iterator tit = types.find(type_id);

    Q_ASSERT(oit != orders.end() && tit != types.end());

    return QCLImageFormat(oit.value(), tit.value());
}





/**
 * @brief       Initialise une image en fonction des paramètres sérialisés
 * @param       res Ressource contenant l'image 2D
 * @param       terrain Terrain
 * @param       data Données sérialisées
 */ 
void setupRessource(Ressource<QCLImage2D>* res,
                    Terrain& terrain, QJsonObject& data)
{

    QCLImageFormat format = getImageFormat(data);

    res->value = terrain.context.createImage2DDevice(
        format, terrain.size(), QCLMemoryObject::ReadWrite);
}

/**
 * @brief       Initialise une image en fonction des paramètres sérialisés
 * @param       res Ressource contenant l'image 3D
 * @param       terrain Terrain
 * @param       data Données sérialisées       
 */ 
void setupRessource(Ressource<QCLImage3D>* res,
                    Terrain& terrain, QJsonObject& data)
{

    QCLImageFormat format = getImageFormat(data);

    QSize size = terrain.size();
    size_t width = size.width(), height = size.height();
    size_t depth = data["depth"].toInt();

    res->value = terrain.context.createImage3DDevice(format,
        width, height, depth, QCLMemoryObject::ReadWrite);   
}

/**
 * @brief       Initialise une image en fonction des paramètres sérialisés
 * @param       res Ressource contenant l'image
 * @param       terrain Terrain
 * @param       data Données sérialisées
 */ 
void setupRessource(Ressource<QCLBuffer>* res,
                    Terrain& terrain, QJsonObject& data)
{

    QString type = data["sizePolicy"].toString("terrain_resolution");
    size_t size;

    if (type == "terrain_resolution") {
        int comp = data["componentSize"].toInt(1);
        QSize terrain_size = terrain.size();
        size = terrain_size.width() * terrain_size.height() * comp;
    }
    else if (type == "fixed")
        size = data["size"].toInt(1);
    else {
        qWarning() << "Unknown size policy :" << type;
        return;
    };
    res->value = terrain.context.createBufferDevice(size,
                                QCLMemoryObject::ReadWrite);
}



/**
 * @brief       Initialise une image en fonction des paramètres sérialisés
 * @param       res Ressource contenant le terrain
 * @param       terrain Terrain
 * @param       data Données sérialisées
 */ 
void setupRessource(TerrainStage::DataPtr res,
                    Terrain& terrain, QJsonObject& data)
{
    QString type = res->type().id;

    typedef QMap<QString, Handler> Handlers;
    
    static const Handlers handlers_map({
        HANDLE_RESSOURCE(QCLImage2D),
        HANDLE_RESSOURCE(QCLImage3D),
        HANDLE_RESSOURCE(QCLBuffer)
    });

    Handlers::const_iterator it = handlers_map.find(type);

    if (Q_LIKELY(it != handlers_map.end())) {
        Handler handler = it.value();
        handler(res.get(), terrain, data);
    }
}






/**
 * @brief       Constructeur de la classe DeclarativeStage
 * @param       terrain Terrain
 * @param       _data Données sérialisées       
 */ 
DeclarativeStage::DeclarativeStage(Terrain& terrain, const QJsonObject& _data):
    TerrainStage(terrain, _data["label"].toString("unknown stage")),
    data(_data),
    widget(new EmbeddedWidget)
{
    QVBoxLayout* layout = new QVBoxLayout;

    widget->setLayout(layout);

    version = data["version"].toString("1.0");
    description = data["description"].toString();

    initInputs();
    initOutputs();
    initRessources();
    initProperties();
    initSubStages();

    layout->addStretch();
}


DeclarativeStage::~DeclarativeStage() {

    root.clear();
}

/**
 * @brief       Initialise une image en fonction des paramètres sérialisés
 * @param       context Ressource contenant l'image       
 */ 
void DeclarativeStage::setup(QCLContext& context) {

    setupProgram();
    setupSubStages();
    setupRessources();

    compute(context);
}

/**
 * @brief       Cherche une entrée dans le nom est passé en paramètre
 * @param       id Id de l'entrée
 */ 
DataPtr DeclarativeStage::getInput(const QString& id) const {

    for (const PortEntry& entry: inputs) {
        QString entry_name(entry.name);

        if (entry_name.replace(' ', '_') != id)
            continue;
        else if (entry.data == Q_NULLPTR)
            return RessourceFactory::getDefault(entry.id);
        
        return entry.data;
    }
    return Q_NULLPTR;
}

/**
 * @brief       Permet de récupérer les ressources d'un noeud
 * @param       id Id du noeud
 */ 
DataPtr DeclarativeStage::getRessource(const QString& id) const {

    Ressources::const_iterator it = ressources.find(id);
    
    if (Q_LIKELY(it != ressources.end()))
        return it.value();

    return Q_NULLPTR;
}

/**
 * @brief       Retourne un le widget à afficher dans le noeud
 */ 
QWidget* DeclarativeStage::embeddedWidget() {
    return widget;
}


QJsonObject DeclarativeStage::save() const {

    QJsonObject object = TerrainStage::save();

    for (const QString& key: ressources.keys())
        object[key] = ressources[key]->serialize();

    return object;
}

void DeclarativeStage::restore(const QJsonObject& data) {

    TerrainStage::restore(data);

    for (const QString& key: ressources.keys())
        ressources[key]->deserialize(data.value(key));

    Q_EMIT updateProperties();
}

void DeclarativeStage::addArg(const QString& arg) {
    args.append(arg);
}

void DeclarativeStage::addArgs(const QStringList& _args) {
    args.append(_args);
}

void DeclarativeStage::registerName(const QString& id, PropConstruct f) {
    prop_constructors[id] = f;
}


void DeclarativeStage::initInputs() {

    QJsonArray inputs_data = data["inputs"].toArray();

    inputs.reserve(inputs_data.size());

    for (const QJsonValue& input_data: inputs_data) {
        QJsonObject object = input_data.toObject();
        
        defineInput(object["name"].toString(),
                    object["type"].toString());
    }
}


void DeclarativeStage::initOutputs() {

    QJsonObject ressources_data = data["ressources"].toObject();
    QJsonArray outputs_data = data["outputs"].toArray();

    outputs.reserve(outputs_data.size());


    for (const QJsonValue& output_data: outputs_data) {
        QJsonObject object = output_data.toObject();

        QString id = object["ressourceID"].toString();
        QJsonObject res = ressources_data[id].toObject();

        defineOutput(object["name"].toString(),
                     res["type"].toString());
    }
}


void DeclarativeStage::initSubStages() {

    QJsonObject root_data = data["exec"].toObject();
    root = SubStageNode::createNode(root_data);
}


void DeclarativeStage::initRessources() {

    QJsonArray outputs_data = data["outputs"].toArray();
    QJsonObject res = data["ressources"].toObject();


    for (const QString& key: res.keys()) {

        QJsonObject value = res[key].toObject();
        QString type = value["type"].toString();

        auto res = RessourceFactory::createRessource(type);
        
        if (Q_LIKELY(res != Q_NULLPTR))
            ressources[key] = qMove(res);
        else
            qWarning() << "unknown ressource type:" << type;
    }


    for (int index=0; index<outputs.size(); ++index) {
        PortEntry& entry = outputs[index];

        QJsonObject res = outputs_data[index].toObject();
        QString id = res["ressourceID"].toString();

        Ressources::const_iterator it = ressources.find(id);

        if (Q_LIKELY(it != ressources.end()))
            entry.data = it.value();
        else
            qWarning() << "undefined ressource:" << id;
    }
}


void DeclarativeStage::setupRessources() {
    
    QJsonObject res = data["ressources"].toObject();
    Ressources::const_iterator it = ressources.begin();

    for (;it != ressources.end(); ++it) {
        QJsonObject res_data = res[it.key()].toObject();
        setupRessource(it.value(), terrain, res_data);
    }
}


void DeclarativeStage::setupSubStages() {
    
    if (Q_LIKELY(root != Q_NULLPTR))
        root->setup(this);
}


void DeclarativeStage::setupProgram() {

    QStringList options(args);

    QString filename = data["program"].toString();
    QJsonArray option_array = data["options"].toArray();

    for (const QJsonValue& val : option_array)
        options.append(val.toString());


    QStringList search_dirs = QDir::searchPaths("cl_shader");
    QFileInfo info("cl_shader:" + filename);

    search_dirs.append(info.absolutePath());

    for (const QString& search_dir: search_dirs)
        options.append("-I" + search_dir);

    program = KernelManager::getProgram(terrain.context,
                                        info.absoluteFilePath(),
                                        options.join(" "));

    // program = terrain.context.buildProgramFromSourceFile(
    //     info.absoluteFilePath(), options.join(" "));
}



void DeclarativeStage::compute(QCLContext& context) {

    if (Q_LIKELY(root != Q_NULLPTR))
        root->exec(Q_NULLPTR);

    for (int i=0; i<outputs.size(); ++i) {
        if (outputs[i].data != Q_NULLPTR)
            Q_EMIT dataUpdated(i);
    }
}


void DeclarativeStage::initProperties() {
    
    QJsonArray props_data = data["properties"].toArray();
    properties.clear();
    properties.reserve(props_data.size());

    for (const QJsonValue& prop: props_data) {
        QJsonObject prop_data = prop.toObject();

        QString widget_name = prop_data["widget"].toString();

        PropConstructors::const_iterator it = 
            prop_constructors.find(widget_name);
        
        if (Q_LIKELY(it != prop_constructors.end())) {

            PropConstruct construct = it.value();
            auto prop = construct(*this, prop_data);

            widget->layout()->addWidget(prop->widget());
            properties.append(move(prop));
        }
        else
            qWarning() << "Undefined property widget type" << widget_name;
    }
}
