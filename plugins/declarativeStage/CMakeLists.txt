cmake_minimum_required(VERSION 3.1)

project(delcarative-stages-plugin LANGUAGES CXX VERSION 1.0)


set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)


find_package(Qt5 COMPONENTS Core Widgets REQUIRED)

set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR})
file(GLOB_RECURSE SRCS ${SRC_DIR}/*.cpp)


add_library(${PROJECT_NAME} SHARED ${SRCS})


target_include_directories(${PROJECT_NAME} PRIVATE ${SRC_DIR}
        ${ALLUVION_INC_DIR}
        ${NODEEDITOR_INCLUDE_DIRS}
        ${QTOPENCL_INCLUDE_DIRS}
        ${QTOPENCLGL_INCLUDE_DIRS}
        ${ZTWIDGETS_INCLUDE_DIRS}
)

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_11)
# target_compile_definitions(${PROJECT_NAME} PRIVATE QT_STATICPLUGIN)

set_target_properties(${PROJECT_NAME}
    PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/plugins"
)


target_link_libraries(${PROJECT_NAME} PRIVATE
    Qt5::Core Qt5::Widgets Qt5::Gui
    nodes qtopencl qtopenclgl
)

add_dependencies(${PROJECT_NAME} ui)

install(TARGETS ${PROJECT_NAME}
    LIBRARY DESTINATION ${PLUGIN_INSTALL_DIR}
    ARCHIVE DESTINATION ${PLUGIN_INSTALL_DIR}
)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/stages
    DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/alluvion
)