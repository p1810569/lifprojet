// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef __KERNELMANAGER_H__
#define __KERNELMANAGER_H__

#include "qclprogram.h"
#include "qclkernel.h"
#include "qclcontext.h"

class KernelManager {

public:
    static QCLProgram getProgram(QCLContext& ctx,
                                const QString& path,
                                const QString& options=QString());

    static QCLKernel getKernel( const QCLProgram& program,
                                const QString& name);

private:
    typedef QMap<QString, QCLProgram> Programs;
    typedef QMap<QString, QCLKernel> Kernels;
    typedef QMap<cl_program, Kernels> KernelMap;

    static Programs programs;
    static KernelMap kernels;
};

#endif // __KERNELMANAGER_H__