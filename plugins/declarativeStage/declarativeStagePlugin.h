// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __DECLARATIVESTAGEPLUGIN_H__
#define __DECLARATIVESTAGEPLUGIN_H__

#include <QFile>
#include "terrainStagePlugin.h"


class DeclarativeStagePlugin: public TerrainStagePlugin {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.alluvion.stage")
    Q_INTERFACES(TerrainStagePlugin)

public:
    DeclarativeStagePlugin() = default;
    virtual ~DeclarativeStagePlugin() = default;

    virtual void registerModel(QtNodes::DataModelRegistry& registery) override;

    static void registerFile(QFile&& path,
                QtNodes::DataModelRegistry& registery);
};


#endif // __DECLARATIVESTAGEPLUGIN_H__