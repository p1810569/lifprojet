// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QStandardPaths>
#include <QJsonDocument>
#include <QDir>

#include "declarativeStage.h"
#include "application.h"
#include "declarativeStagePlugin.h"

using namespace QtNodes;


std::unique_ptr<NodeDataModel> create(const QJsonObject& data) {

    Application* app = Application::globalInstance();
    return std::make_unique<DeclarativeStage>(app->terrain, data);
}


void DeclarativeStagePlugin::registerModel(DataModelRegistry& registery) {
    
    Application* app = Application::globalInstance();
    
    static QString default_path = QStandardPaths::locate(
                QStandardPaths::AppDataLocation, "stages",
                QStandardPaths::LocateDirectory);
    
    QVariant path_value = app->settings.value("stages/path", default_path);
    QDir path(path_value.toString());

    QDir::addSearchPath("cl_shader", path.absolutePath());

    if (!path.isEmpty() && path.exists()) {

        for (QString basename: path.entryList(QDir::Files)) {
            QFile file(path.absoluteFilePath(basename));
            QFileInfo info(file);

            if (Q_LIKELY(info.suffix() == "json"))
                registerFile(qMove(file), registery);
        }
    }
}


void DeclarativeStagePlugin::registerFile(QFile&& file,
                            DataModelRegistry& registery)
{
    if (file.open(QIODevice::ReadOnly)) {
        QJsonDocument doc(QJsonDocument::fromJson(file.readAll()));
        QJsonObject object = doc.object();

        QString category = object["category"].toString("node");
        auto creator = std::bind(create, qMove(object));

        registery.registerModel(qMove(creator), qMove(category));

        file.close();
    }
}
