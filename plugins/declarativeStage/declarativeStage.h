// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __DECLARATIVESTAGE_H__
#define __DECLARATIVESTAGE_H__

#include <QSlider>
#include <QDoubleSpinBox>
#include <type_traits>
#include <qclkernel.h>
#include <qclprogram.h>

#include "subStageNode.h"
#include "ressourceData.h"
#include "utils.h"
#include "terrainStage.h"


class PropertyInterface;


/**
 * @file        declarativeStage.h
 * @brief       Contient la déclaration de la classe DeclarativeStage
 * @details     La classe \c DeclarativeStage permet de montrer l'utilisation des \em tags \b Doxygen
 * @author      Matthieu Jacquemet <Matthieu.jacquemet@univ-lyon1.fr>
 * @date        2021
 */
class DeclarativeStage: public TerrainStage {

    Q_OBJECT

public:
    DeclarativeStage(Terrain& terrain, const QJsonObject& data);
    virtual ~DeclarativeStage();

    virtual void compute(QCLContext& context) override;
    virtual void setup(QCLContext& context) override;

    DataPtr getInput(const QString& id) const;
    DataPtr getRessource(const QString& id) const;

    virtual QWidget* embeddedWidget() override;

    virtual QJsonObject save() const override;
    virtual void restore(const QJsonObject& data) override;

    typedef QSharedPointer<PropertyInterface> PropRef;
    typedef PropRef PropConstFn(DeclarativeStage&, const QJsonObject&);
    typedef std::function<PropConstFn> PropConstruct;
    
    static void addArg(const QString& arg);
    static void addArgs(const QStringList& args);
    static void registerName(const QString& id, PropConstruct f);

protected:
    void initInputs();
    void initOutputs();
    void initRessources();
    void initProperties();
    void initSubStages();

    void setupRessources();
    void setupSubStages();
    void setupProgram();


    typedef QVector<QSharedPointer<PropertyInterface>> Properties;
    typedef QMap<QString, PropConstruct> PropConstructors;
    typedef QMap<QString, DataPtr> Ressources;
    typedef SubStageNode::NodeRef ExecNode;

    Properties properties;
    QCLProgram program;
    Ressources ressources;
    QJsonObject data;
    ExecNode root;

    QWidget* widget;


    static PropConstructors prop_constructors;
    static QStringList args;

    friend class SubStage;

Q_SIGNALS:
    void updateProperties() const;
};


#endif // __DECLARATIVESTAGE_H__