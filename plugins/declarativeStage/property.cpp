// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <QLabel>
#include <QGraphicsView>
#include <QHBoxLayout>

#include "property.h"




#define REGISTER_PROPERTY(DISPLAY_TYPE, DATA_TYPE) template<> \
    struct PropertyType<DISPLAY_TYPE> { using type = DATA_TYPE; }; \
    INITIALIZER(prop) { Property<DISPLAY_TYPE>::registerName(#DISPLAY_TYPE); }



REGISTER_PROPERTY(ComboBox, cl_uint)
REGISTER_PROPERTY(Slider, float)
REGISTER_PROPERTY(DoubleSpinBox, float)
REGISTER_PROPERTY(IntSpinBox, cl_int)
REGISTER_PROPERTY(SpinBoxVector2D, QVector2D)
REGISTER_PROPERTY(SpinBoxVector3D, QVector3D)
REGISTER_PROPERTY(SpinBoxVector4D, QVector4D)



void setup(const QJsonObject& data, Property<ComboBox>* prop, ComboBox* widget) {

    QJsonArray enum_items = data["values"].toArray();

    for (const QJsonValue& value: enum_items)
        widget->addItem(value.toString());

    CONNECT(prop, currentIndexChanged, int);
}


void setup(const QJsonObject& data, Property<DoubleSpinBox>* prop, DoubleSpinBox* widget) {

    double min = data["min"].toDouble(-INFINITY);
    double max = data["max"].toDouble(INFINITY);
    double default_value = data["default"].toDouble(0.0);

    widget->setRange(min, max);
    widget->setValue(default_value);
    widget->setSingleStep(0.01);

    CONNECT(prop, valueChanged, double)
}


void setup(const QJsonObject& data, Property<IntSpinBox>* prop, IntSpinBox* widget) {

    int min = data["min"].toInt(INT_MIN);
    int max = data["max"].toInt(INT_MAX);
    int default_value = data["default"].toInt(0.0);

    widget->setRange(min, max);
    widget->setValue(default_value);
    widget->setSingleStep(1);

    CONNECT(prop, valueChanged, int)
}


void setup(const QJsonObject& data, Property<Slider>* prop, Slider* widget) {

    widget->setMinimum(data["min"].toDouble(0.0));
    widget->setMaximum(data["max"].toDouble(1.0));
    widget->setValue( data["default"].toDouble(0.0));

    widget->setSingleStep(0.1);
    widget->setOrientation(Qt::Horizontal);

    CONNECT(prop, valueChanging, const qreal&)
}






PropertyInterface::PropertyInterface(DeclarativeStage& _stage,
                                    QWidget* widget,
                                    QString _label):
    label_text(qMove(_label)),
    prop_widget(new QWidget),
    stage(_stage)
{
    prop_widget->setLayout(new QHBoxLayout);
    prop_widget->layout()->addWidget(new QLabel(label_text));
    prop_widget->layout()->addWidget(widget);
}


QWidget* PropertyInterface::widget() const {
    return prop_widget;
}
