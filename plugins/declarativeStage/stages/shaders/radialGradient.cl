#include "utils.cl"

#define TYPE_GAUSS 0
#define TYPE_LINEA 1
#define TYPE_SPHER 2


float gaussian(float2 p) {
    return exp(dot(p,p) * -7.389056f);
}

float linear(float2 p) {
    return max(1.f - length(p), 0.f);
}

float spherical(float2 p) {
    return max(sin(acos(length(p))), 0.0f);
}


__kernel void gradient( const float radius,
                        const float gain,
                        const float2 offset,
                        const uint type,
                        __write_only image2d_t output)
{

    int2 ocoord = get_image_dim(output);
    float2 p = get_work_item_coord(&ocoord);

    p = (p - offset) / radius;

    float result = 0.f;

    switch (type) {
        case TYPE_GAUSS: result = gaussian(p); break;
        case TYPE_LINEA: result = linear(p); break;
        case TYPE_SPHER: result = spherical(p); break;
    }

    write_imagef(output, ocoord, result * gain);
}

