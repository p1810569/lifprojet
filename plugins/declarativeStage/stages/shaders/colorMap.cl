// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __COLORMAP_CL__
#define __COLORMAP_CL__

float3 colormap_jet(float x) {

    float3 color;
    float a = x * 4.f;
    color.r = clamp(mix(a - 1.5f, -a + 4.5f, step(.7f, x)), 0.f, 1.f);
    color.g = clamp(mix(a - .5f, -a + 3.5f, step(.5f, x)), 0.f, 1.f);
    color.b = clamp(mix(a + .5f, -a + 2.5f, step(.3f, x)), 0.f, 1.f);

    return color;
}

float3 colormap_simple(float value) {

    float3 color;
    float t = 2.f - value * 4.f;
    color.g = clamp(2 - fabs(t), 0.f, 1.f);
    color.r = clamp(-t, 0.f, 1.f);
    color.b = clamp(t, 0.f, 1.f);

    return color;
}


#endif //__COLORMAP_CL__