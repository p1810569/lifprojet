#include "utils.cl"
#include "hash.cl"



__kernel void compute_color(read_only image2d_t in_height,
                            read_only image2d_t in_wear,
                            read_only image2d_t in_base,
                            read_only image2d_t in_sediment,
                            read_only image2d_t in_steep,
                            write_only image2d_t res_output)
{

    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    int2 size = (int2)(get_global_size(0), get_global_size(1));

    const sampler_t smp =   CLK_ADDRESS_CLAMP_TO_EDGE |
                            CLK_FILTER_NEAREST |
                            CLK_NORMALIZED_COORDS_FALSE;

    const sampler_t smp2 =  CLK_ADDRESS_REPEAT |
                            CLK_FILTER_LINEAR |
                            CLK_NORMALIZED_COORDS_TRUE;

    float h = read_imagef(in_height, smp, coord).x;
    float w = read_imagef(in_wear, smp, coord).x;

    float2 tex_coord = convert_float2(coord) / convert_float2(size);
    tex_coord *= 4.f;

    float3 base_c = read_imagef(in_base, smp2, tex_coord).xyz;
    float3 steep_c = read_imagef(in_steep, smp2, tex_coord).xyz;
    float3 sediment_c = read_imagef(in_sediment, smp2, tex_coord).xyz;

    float3 n = compute_normal(h, coord, in_height);
    float fact = smoothstep(.1f, .15f, asin(n.z) / M_PI_2_F);


    float3 c = mix(steep_c, base_c, clamp(fact, 0.f, 1.f));
    c = mix(c, sediment_c, min(w * 7.f, 1.f));

    // const float3 grass = (float3)(0.345f, 0.556f, 0.223f);
    // const float3 rock = (float3)(0.752f, 0.741f, 0.666f);
    // const float3 sediment = (float3)(0.839f, 0.952f, 0.286f);

    // float3 c = (float3)(0.584f, 0.325f, 0.196f);
    // float3 sand = (float3)(0.976f, 0.870f, 0.482f);
    // c = mix(c, sand, min(w * 7.f, 1.f));

    write_imagef(res_output, coord, (float4)(c, 0.f));
}

