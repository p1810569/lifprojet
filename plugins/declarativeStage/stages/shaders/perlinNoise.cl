#include "matrix.cl"
#include "utils.cl"
#include "hash.cl"


#define TYPE_NORMAL 0
#define TYPE_RIDGED 1
#define TYPE_BILLOW 2

#define COMP_OP_ADD 0
#define COMP_OP_MUL 1
#define COMP_OP_HYB 2


float simplex_noise(float2 p, int seed) {

    const float K1 = 0.366025404f; // (sqrt(3)-1)/2;
    const float K2 = 0.211324865f; // (3-sqrt(3))/6;

	float2 i = floor(p + (p.x + p.y) * K1);
    float2 a = p - i + (i.x + i.y) * K2;
    float  m = step(a.y, a.x); 
    float2 o = (float2)(m, 1.f - m);
    float2 b = a - o + K2;
	float2 c = a - 1.f + 2.f * K2;

    float3 h = max(.5f - (float3)(dot(a, a), dot(b, b), dot(c, c)), 0.f);

	float3 n = h * h * h * h * (float3)(
        dot(a, fma(hash22(i + seed), 2.f, -1.f)),
        dot(b, fma(hash22(i + o + seed), 2.f, -1.f)),
        dot(c, fma(hash22(i + 1.f + seed), 2.f, -1.f))
    );

    return dot(n, 70.0f);
}



float smooth_abs(float x, float t) {
    float offset = sqrt(t + 1.f);
    return 1.f - (offset - almost_identity(x, t)) * (sqrt(t) + offset);
}


void get_noise(float2 p, int seed, float f, float s,
                uint type, uint comp_op, float offset,
                float* weight, float* result)
{

    float value = simplex_noise(p, seed);
    
    switch (type) {
        case TYPE_NORMAL: value = fma(value, .5f, .5f); break;
        case TYPE_RIDGED: value = 1.f - smooth_abs(value, s); break;
        case TYPE_BILLOW: value = smooth_abs(value, s);
    }
    value = value + offset;

    switch (comp_op) {
        case COMP_OP_ADD: *result += value * f; break;
        case COMP_OP_MUL: *result *= value * f + 1.f; break;
        case COMP_OP_HYB: {
            float signal = value * f;
            *result += signal * *weight;
            *weight *= signal;
        }
    }
}



__kernel void noise(const uint type,
                    const uint comp_op,
                    const float scale,
                    const float gain,
                    const float offset,
                    const float weight,
                    const float smoothness,
                    const float lacunarity,
                    const float persistency,
                    const int seed,
                    const float octaves,
                    __read_only image2d_t in_domain_warping,
                    __write_only image2d_t output)
{

    int2 ocoord = get_image_dim(output);
    float2 p = get_work_item_coord(&ocoord) * scale;

    const sampler_t smp =   CLK_NORMALIZED_COORDS_TRUE |
                            CLK_ADDRESS_CLAMP_TO_EDGE |
                            CLK_FILTER_LINEAR;

    float3 warp = read_imagef(in_domain_warping, smp, fma(p, .5f, .5f)).xyz;

    float w = weight;
    float result = comp_op == COMP_OP_MUL ? 1.f : 0.f;
    float integer_part;
    float fractional = fract(octaves, &integer_part);
    float s = smoothness * smoothness * smoothness;
    float f = 1.0;
    p += warp.xy;

    mat2 m = mat2_new(.866f, -.5f, .5f, .866f);
    m = mat2_mul_scalar(m, lacunarity);

    int n = (int)integer_part;

    for (int i=0; i<n; ++i) {
        
        get_noise(p, seed, f, s, type, comp_op, offset, &w, &result);
    
        p = mat2_mul_f2(m, p);
        f *= persistency;
        s /= lacunarity;
    }
    f *= fractional;
    get_noise(p, seed, f, s, type, comp_op, offset, &w, &result);

    result -= comp_op == COMP_OP_MUL ? 1.f : 0.f;

    result = fma(result, .5f, .5f) * gain;
    write_imagef(output, ocoord, result);
}













// float2 hash(float2 p) {

// 	p = (float2)(dot(p, (float2)(127.1f, 311.7f)),
//                  dot(p, (float2)(269.5f, 183.3f)));

// 	return fma(fract(fabs(cos(p)) * 43758.5453123f, &p), 2.f, -1.f);
// }


// float3 permute(float3 x, float seed) {
//     return fmod(((x * 34.f) + 1.f) * x + seed, 289.f);
// }

// float simplex_noise(float2 v, float seed) {

//     const float4 C = (float4)(.211324865405187f, .366025403784439f,
//                             -.577350269189626f, .024390243902439f);

//     v += seed * 100000.f;
//     float2 i = floor(v + dot(v, C.yy) );
//     float2 x0 = v - i + dot(i, C.xx);
//     float2 i1;
//     i1 = (x0.x > x0.y) ? (float2)(1.f, 0.f) : (float2)(0.f, 1.f);

//     float4 x12 = x0.xyxy + C.xxzz;
//     x12.xy -= i1;
//     i = fmod(i, 289.f);
//     float3 p = permute(permute(i.y + (float3)(0.f, i1.y, 1.f ), seed)
//                              + i.x + (float3)(0.f, i1.x, 1.f), seed);

//     float3 m = max(.5f - (float3)(  dot(x0, x0),
//                                     dot(x12.xy, x12.xy),
//                                     dot(x12.zw, x12.zw)), 0.f);
//     m = m*m;
//     m = m*m;

//     float3 null;
//     float3 x = 2.f * fract(p * C.www, &null) - 1.f;
//     float3 h = fabs(x) - .5f;
//     float3 ox = floor(x + .5f);
//     float3 a0 = x - ox;
//     m *= 1.79284291400159f - 0.85373472095314f * (a0 * a0 + h*h);
//     float3 g;
//     g.x  = a0.x  * x0.x  + h.x  * x0.y;
//     g.yz = a0.yz * x12.xz + h.yz * x12.yw;

//     return 130.f * dot(m, g);
// }
