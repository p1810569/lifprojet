// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __MATRIX_CL__
#define __MATRIX_CL__


#define VECTOR_SUBSCRIPTEN_ACCESS(N) \
    union Vector##N { \
        float data[N]; \
        float##N vector; \
    };
    

#define DEFINE_MATRIX_VECTOR_MUL(N) \
    float##N mat##N##_mul_##f##N(mat##N m, float##N v) { \
        union Vector##N ret; \
        for (int i=0; i<N; ++i) \
            ret.data[i] = dot(float##N##_new(m.data[i]), v); \
        return ret.vector; \
    }


#define DEFINE_MATRIX_DATA_STRUCT(N) \
    typedef struct { float data[N][N]; } mat##N; \



#define DEFINE_MATRIX_COL_ACCESS(N) \
    float##N mat##N##_get_col(mat##N m, int n) { \
        union Vector##N ret, row; \
        for (int i=0; i<N; ++i) \
            ret.data[i] = m.data[i][n]; \
        return ret.vector; \
    }


#define DEFINE_MATRIX_MATRIX_MUL(N) \
    mat##N mat##N##_mul_##mat##N(mat##N m1, mat##N m2) { \
        mat##N ret; \
        for (int i=0; i<N; ++i) { \
            for (int j=0; j<N; ++j) \
                ret.data[i][j] = dot(float##N##_new(m1.data[i]), \
                        mat##N##_get_col(m2, j)); \
        } \
        return ret; \
    }


#define DEFINE_MATRIX_TRANSPODE(N) \
    mat##N mat##N##_transpose(mat##N m) { \
        mat##N ret; \
        for (int i=0; i<N; ++i) { \
            for (int j=0; j<N; ++j) \
                ret.data[i][j] = m.data[j][i]; \
        } \
        return ret; \
    }


#define DEFINE_MATRIX_SCALAR_MUL(N) \
    mat##N mat##N##_mul_scalar(mat##N m, float n) { \
        for (int i=0; i<N; ++i) { \
            for (int j=0; j<N; ++j) \
                m.data[i][j] *= n; \
        } \
        return m; \
    }


#define DEFINE_MATRIX(N) \
    VECTOR_SUBSCRIPTEN_ACCESS(N) \
    DEFINE_MATRIX_DATA_STRUCT(N) \
    DEFINE_MATRIX_COL_ACCESS(N) \
    DEFINE_MATRIX_SCALAR_MUL(N) \
    DEFINE_MATRIX_VECTOR_MUL(N) \
    DEFINE_MATRIX_MATRIX_MUL(N) \
    DEFINE_MATRIX_TRANSPODE(N)



float4 float4_new(float* data) {
    return (float4)(data[0], data[1], data[2], data[3]);
}

float3 float3_new(float* data) {
    return (float3)(data[0], data[1], data[2]);
}

float2 float2_new(float* data) {
    return (float2)(data[0], data[1]);
}


DEFINE_MATRIX(2)
DEFINE_MATRIX(3)
DEFINE_MATRIX(4)



mat4 mat4_new(float xx, float xy, float xz, float xw,
              float yx, float yy, float yz, float yw,
              float zx, float zy, float zz, float zw,
              float wx, float wy, float wz, float ww) {
    mat4 m;
    m.data[0][0] = xx; m.data[0][1] = xy; m.data[0][2] = xz; m.data[0][3] = xw;
    m.data[1][0] = yx; m.data[1][1] = yy; m.data[1][2] = yz; m.data[1][3] = yw;
    m.data[2][0] = zx; m.data[2][1] = zy; m.data[2][2] = zz; m.data[2][3] = zw;
    m.data[3][0] = wx; m.data[3][1] = wy; m.data[3][2] = wz; m.data[3][3] = ww;
    return m;
}

mat3 mat3_new(float xx, float xy, float xz,
              float yx, float yy, float yz,
              float zx, float zy, float zz) {
    mat3 m;
    m.data[0][0] = xx; m.data[0][1] = xy; m.data[0][2] = xz;
    m.data[1][0] = yx; m.data[1][1] = yy; m.data[1][2] = yz;
    m.data[2][0] = zx; m.data[2][1] = zy; m.data[2][2] = zz;
    return m;
}


mat2 mat2_new(float xx, float xy,
              float yx, float yy) {
    mat2 m;
    m.data[0][0] = xx; m.data[0][1] = xy;
    m.data[1][0] = yx; m.data[1][1] = yy;
    return m;
}


mat4 mat4_id() {
    return mat4_new(1,0,0,0,
                    0,1,0,0,
                    0,0,1,0,
                    0,0,0,1);
}

mat3 mat3_id() {
    return mat3_new(1,0,0,
                    0,1,0,
                    0,0,1);
}

mat2 mat2_id() {
    return mat2_new(1,0,
                    0,1);
}



#endif // __MATRIX_CL__