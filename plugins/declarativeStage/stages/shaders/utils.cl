#ifndef __UTILS_CL__
#define __UTILS_CL__


float2 get_work_item_coord(int2* wi_coord) {
    
    float2 size = convert_float2(*wi_coord);
    *wi_coord = (int2)(get_global_id(0), get_global_id(1));
    float2 p = convert_float2(*wi_coord) / min(size.x, size.y);

    return fma(p, 2.f, -1.f);
}


float almost_identity(float x, float n) {
    return sqrt(x * x + n);
}

float3 compute_normal(float z, int2 p, image2d_t img) { 

    const sampler_t smp = CLK_NORMALIZED_COORDS_FALSE |
                        CLK_ADDRESS_CLAMP_TO_EDGE |
                        CLK_FILTER_NEAREST;

	float zx = read_imagef(img, smp, p + (int2)(1,0)).x;
	float zy = read_imagef(img, smp, p + (int2)(0,1)).x;
    int2 size = get_image_dim(img);

	float2 dxy = (float2)(z - zx, z - zy) * convert_float2(size);

	return normalize((float3)(dxy, 1.f));
}


#endif // __UTILS_CL__