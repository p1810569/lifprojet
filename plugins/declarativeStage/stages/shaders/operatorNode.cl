const sampler_t smp =  CLK_NORMALIZED_COORDS_TRUE | \
                        CLK_ADDRESS_CLAMP_TO_EDGE | \
                        CLK_FILTER_LINEAR; \



#define OPERATOR2(OP) \
    __kernel void OP(__read_only image2d_t input1, \
                    __read_only image2d_t input2, \
                    __write_only image2d_t output) \
    { \
        int2 ocoord = (int2)(get_global_id(0), get_global_id(1)); \
        float2 size = convert_float2(get_image_dim(output)); \
        float2 icoout = convert_float2(ocoord) / size; \
        \
        float4 pixel1 = read_imagef(input1, smp, icoout); \
        float4 pixel2 = read_imagef(input2, smp, icoout); \
        \
        float4 res = op_##OP(pixel1, pixel2); \
        \
        write_imagef(output, ocoord, res); \
    }


#define OPERATOR3(OP) \
    __kernel void OP(__read_only image2d_t input1, \
                        __read_only image2d_t input2, \
                        __read_only image2d_t input3, \
                        __write_only image2d_t output) \
    { \
        int2 coord = (int2)(get_global_id(0), get_global_id(1)); \
        int2 size = get_image_dim(input1); \
        \
        uint4 pixel1 = read_imageui(input1, smp, coord); \
        uint4 pixel2 = read_imageui(input2, smp, coord); \
        uint4 pixel3 = read_imageui(input3, smp, coord); \
        \
        float4 f1 = convert_float4(pixel1); \
        float4 f2 = convert_float4(pixel2); \
        float4 f3 = convert_float4(pixel3); \
        \
        float4 res = op_##OP(f1, f2, f3); \
        \
        uint4 value = convert_uint4(res); \
        \
        write_imageui(output, coord, value); \
    }

float4 op_add(float4 x, float4 y) {
    return x + y;
}

float4 op_mul(float4 x, float4 y) {
    return x * y;
}

float4 op_div(float4 x, float4 y) {
    return x / y;
}

float4 op_mix1(float4 x, float4 y, float4 z) {
    return mix(x,y,z);
}

OPERATOR2(add)
OPERATOR2(mul)
OPERATOR2(div)
OPERATOR3(mix1)

// __kernel void add(  __read_only image2d_t input,
//                     __write_only image2d_t output,
//                     float value,
//                     float var)
// {
//     int2 gid = (int2)(get_global_id(0), get_global_id(1));
//     int2 size = get_image_dim(input);

//     uint4 pixel = read_imageui(input, sampler, gid);
//     value += convert_float(pixel.x);

//     uint4 computed_value = convert_uint(value);

//     write_imageui(output, gid, computed_value);
// }