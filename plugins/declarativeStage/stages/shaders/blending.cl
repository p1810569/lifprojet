// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



const sampler_t smp =  CLK_NORMALIZED_COORDS_TRUE |
                        CLK_ADDRESS_CLAMP_TO_EDGE |
                        CLK_FILTER_LINEAR;

#define OP_MIX 0
#define OP_ADD 1
#define OP_SUB 2
#define OP_MUL 3
#define OP_DIV 4
#define OP_SCR 5
#define OP_OVL 6


float4 screen(float4 a, float4 b) {
    return 1.f - (1.f - a) * (1.f - b);
}

float4 overlay(float4 a, float4 b) {
    return mix(1.f - 2.f * (1.f - a) * (1.f - b), 2.f * a * b, step(a, 0.5f));
}


__kernel void blend(__read_only image2d_t in_a,
                    __read_only image2d_t in_b,
                    __read_only image2d_t in_factor,
                    __write_only image2d_t res_output,
                    uint res_type)
{
    int2 ocoord = (int2)(get_global_id(0), get_global_id(1));
    float2 size = convert_float2(get_image_dim(res_output));
    float2 icoord = convert_float2(ocoord) / size;
   
    float4 pixel1 = read_imagef(in_a, smp, icoord);
    float4 pixel2 = read_imagef(in_b, smp, icoord);
    float factor = read_imagef(in_factor, smp, icoord).x;

    float4 res = 0.0f;

    switch (res_type) {
        case OP_MIX: res = mix(pixel1, pixel2, factor); break;
        case OP_ADD: res = fma(pixel2, factor, pixel1); break;
        case OP_SUB: res = fma(-pixel2, factor, pixel2); break;
        case OP_MUL: res = pixel1 * mix(1.f, pixel2, factor); break;
        case OP_DIV: res = pixel1 / mix(1.f, pixel2, factor); break;
        case OP_SCR: res = mix(pixel1, screen(pixel1, pixel2), factor); break;
        case OP_OVL: res = mix(pixel1, overlay(pixel1, pixel2), factor); break;
        default: res = pixel1;
    }
   
    write_imagef(res_output, ocoord, res);
}