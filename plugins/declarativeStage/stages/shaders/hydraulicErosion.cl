#include "utils.cl"
#include "hash.cl"



struct Data {
    float height;
    float water;
    float sediment;
    float min_height;
    float max_height;
    int water_inflow;
    int sediment_inflow;
    int flow;
};



__kernel void finalize( const int res_iter,
                        global struct Data* buffer,
                        write_only image2d_t heightfield,
                        write_only image2d_t wear)
{

    int2 coord = (int2)(get_global_id(0), get_global_id(1));

    uint offset = get_global_size(0) * coord.y + coord.x;
    struct Data cell = buffer[offset];

    float flow = (float)cell.flow / (float)0xFFFFFF;
    flow = flow * 200.f / (float)res_iter;

    write_imagef(heightfield, coord, (float4)(cell.height));
    write_imagef(wear, coord, (float4)(flow));
}





__kernel void add_water(const float Kr,
                        const float Kv,
                        const float Ks,
                        const uint env_current_iteration,
                        global struct Data* buffer,
                        read_only image2d_t in_rainmap)
{

    int2 coord = (int2)(get_global_id(0), get_global_id(1));

    int offset = get_global_size(0) * coord.y + coord.x;
    global struct Data* data = buffer + offset;

    const sampler_t smp =   CLK_NORMALIZED_COORDS_FALSE |
                            CLK_ADDRESS_CLAMP_TO_EDGE |
                            CLK_FILTER_NEAREST;

    float3 seed = (float3)(convert_float2(coord), env_current_iteration);
    float density = read_imagef(in_rainmap, smp, coord).x;

    data->water += (1.f - hash13(seed) * Kv) * Kr * density * 0.01f;
    data->height -= Ks;
}






__kernel void erosion(  const float Ke,
                        const float Kc,
                        const float Ks,
                        const float Kd,
                        global struct Data* buffer)
{

    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    int2 size = (int2)(get_global_size(0), get_global_size(1));

    uint offset = get_global_size(0) * coord.y + coord.x;
    __global struct Data* data = buffer + offset;

    float water_flow = (float)data->water_inflow / (float)0xFFFFFF;
    float sediment_flow = (float)data->sediment_inflow / (float)0xFFFFFF;

    float water = max(0.f, data->water + water_flow);



    int start_x = max(coord.x - 1, 0);
    int start_y = max(coord.y - 1, 0);

    int end_x = min(coord.x + 1, size.x - 1);
    int end_y = min(coord.y + 1, size.y - 1);

    // evaporation
    float water_loss = water * Ke;

    // sediment transfere
    // float sediment = (sediment_flow * Kd) - (water_loss * Ks);
    float sediment = (water_loss * Ks);

    data->height += sediment;
    // data->height = max(data->height + sediment, data->min_height);
    data->sediment = max(data->sediment - sediment, 0.f);
    data->water = water - water_loss;
}






__kernel void movement(volatile global struct Data* buffer) {


    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    int2 size = (int2)(get_global_size(0), get_global_size(1));

    int offset = size.x * coord.y + coord.x;
    volatile global struct Data* data = buffer + offset;

    float current_water = data->water;
    float current_height = data->height;
    float current_sediment = data->sediment;

    float current_effective_height = current_height + current_water;
    float total_diff_count = 0.f;
    float min_height = 10000.0f;
    float max_height = -10000.0f;

    int start_x = max(coord.x - 1, 0);
    int start_y = max(coord.y - 1, 0);

    int end_x = min(coord.x + 1, size.x - 1);
    int end_y = min(coord.y + 1, size.y - 1);


    for (int y = start_y; y <= end_y; ++y) {
        for (int x = start_x; x <= end_x; ++x) {
            
            if (x == 0 && y == 0) continue;

            struct Data cell = buffer[size.x * y + x];

            float cell_effective_height = cell.height + cell.water;
            // float diff = current_height - cell.height;
            float diff = current_effective_height - cell_effective_height;

            min_height = min(min_height, cell.height);
            max_height = max(max_height, cell.height);

            total_diff_count += max(0.f, diff);
        }
    }

    if (total_diff_count == 0.0)
        return;

    // total_diff_count = max(1.f, total_diff_count);
    float total_water_removed = 0.f;
    float total_sediment_removed = 0.f;


    for (int y = start_y; y <= end_y; ++y) {
        for (int x = start_x; x <= end_x; ++x) {

            if (x == 0 && y == 0) continue;

            int cell_offset = size.x * y + x;
            volatile global struct Data* cell = buffer + cell_offset;

            float cell_effective_height = cell->height + cell->water;
            // float diff = current_height - cell->height;
            float diff = current_effective_height - cell_effective_height;

            if (diff <= 0.0f) continue;

            float w_gain = 0.0f;
            float s_gain = 0.0f;
            
            float factor = diff / total_diff_count;

            if (current_water < diff) {
                w_gain = current_water * factor; 
                s_gain = current_sediment * factor; 
            } else {
                w_gain = (diff * .5f) * factor;
                s_gain = (diff * .5f) * factor;
            }

            total_water_removed += w_gain;
            total_sediment_removed += s_gain;

            int w_gain_as_int = (int)(w_gain * (float)0xFFFFFF);
            int s_gain_as_int = (int)(s_gain * (float)0xFFFFFF);

            atomic_add(&cell->water_inflow, w_gain_as_int);
            atomic_add(&cell->sediment_inflow, s_gain_as_int);

            atomic_add(&cell->flow, w_gain_as_int);
        }    
    }

    int water_removed = (int)(total_water_removed * (float)0xFFFFFF);
    int sediment_removed = (int)(total_sediment_removed * (float)0xFFFFFF);

    atomic_add(&data->water_inflow, -water_removed);
    atomic_add(&data->sediment_inflow, -sediment_removed);

    data->min_height = min_height;
    data->max_height = max_height;
}






__kernel void init( global struct Data* buffer,
                    read_only image2d_t in_height)
{

    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    uint offset = get_global_size(0) * coord.y + coord.x;

    const sampler_t smp =   CLK_NORMALIZED_COORDS_FALSE |
                            CLK_ADDRESS_CLAMP_TO_EDGE |
                            CLK_FILTER_NEAREST;
    
    buffer[offset].height = read_imagef(in_height, smp, coord).x;
    buffer[offset].water = 0.f;
    buffer[offset].sediment = 0.f;
    buffer[offset].flow = 0;
    buffer[offset].water_inflow = 0;
    buffer[offset].sediment_inflow = 0;

}
