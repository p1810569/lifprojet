// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __HASH_CL__
#define __HASH_CL__

#define NULLS float null1; float2 null2; float3 null3; float4 null4;

//------------------------------------------------------------------------------
//  1 out, 1 in...
float hash11(float p) {
    NULLS

    p = fract(p * .1031f, &null1);
    p *= p + 33.33f;
    p *= p + p;
    return fract(p, &null1);
}

//------------------------------------------------------------------------------
//  1 out, 2 in...
float hash12(float2 p) {
    NULLS
    
    float3 p3 = fract((float3)(p.xyx) * .1031f, &null3);
    p3 += dot(p3, p3.yzx + 33.33f);
    return fract((p3.x + p3.y) * p3.z, &null1);
}

//------------------------------------------------------------------------------
//  1 out, 3 in...
float hash13(float3 p3) {
    NULLS
	
    p3 = fract(p3 * .1031f, &null3);
    p3 += dot(p3, p3.zyx + 31.32f);
    return fract((p3.x + p3.y) * p3.z, &null1);
}

//------------------------------------------------------------------------------
//  2 out, 1 in...
float2 hash21(float p) {
    NULLS
    
    float3 p3 = fract((float3)(p) * (float3)(.1031f, .1030f, .0973f), &null3);
	p3 += dot(p3, p3.yzx + 33.33f);
    return fract((p3.xx + p3.yz) * p3.zy, &null2);
}

//------------------------------------------------------------------------------
///  2 out, 2 in...
float2 hash22(float2 p) {
    NULLS
	
    float3 p3 = fract((float3)(p.xyx) * (float3)(.1031f, .1030f, .0973f), &null3);
    p3 += dot(p3, p3.yzx + (float3)(33.33f));
    return fract((p3.xx + p3.yz) * p3.zy, &null2);
}

//------------------------------------------------------------------------------
///  2 out, 3 in...
float2 hash23(float3 p3) {
    NULLS
	
    p3 = fract(p3 * (float3)(.1031f, .1030f, .0973f), &null3);
    p3 += dot(p3, p3.yzx + 33.33f);
    return fract((p3.xx + p3.yz) * p3.zy, &null2);
}

//------------------------------------------------------------------------------
//  3 out, 1 in...
float3 hash31(float p) {
    NULLS
    
    float3 p3 = fract((float3)(p) * (float3)(.1031f, .1030f, .0973f), &null3);
    p3 += dot(p3, p3.yzx + 33.33f);
    return fract((p3.xxy + p3.yzz) * p3.zyx, &null3);
}


//------------------------------------------------------------------------------
///  3 out, 2 in...
float3 hash32(float2 p) {
    NULLS
	
    float3 p3 = fract((float3)(p.xyx) * (float3)(.1031f, .1030f, .0973f), &null3);
    p3 += dot(p3, p3.yxz + 33.33f);
    return fract((p3.xxy + p3.yzz) * p3.zyx, &null3);
}

//------------------------------------------------------------------------------
///  3 out, 3 in...
float3 hash33(float3 p3) {
    NULLS

	p3 = fract(p3 * (float3)(.1031f, .1030f, .0973f), &null3);
    p3 += dot(p3, p3.yxz + 33.33f);
    return fract((p3.xxy + p3.yxx) * p3.zyx, &null3);
}   

//------------------------------------------------------------------------------
// 4 out, 1 in...
float4 hash41(float p) {
    NULLS
    
    float4 p4 = fract((float4)(p) * (float4)(.1031f, .1030f, .0973f, .1099f), &null4);
    p4 += dot(p4, p4.wzxy + 33.33f);
    return fract((p4.xxyz + p4.yzzw) * p4.zywx, &null4);
}

//------------------------------------------------------------------------------
// 4 out, 2 in...
float4 hash42(float2 p) {
    NULLS
	
    float4 p4 = fract((float4)(p.xyxy) * (float4)(.1031f, .1030f, .0973f, .1099f), &null4);
    p4 += dot(p4, p4.wzxy + 33.33f);
    return fract((p4.xxyz + p4.yzzw) * p4.zywx, &null4);
}

//------------------------------------------------------------------------------
// 4 out, 3 in...
float4 hash43(float3 p) {
    NULLS
	
    float4 p4 = fract((float4)(p.xyzx) * (float4)(.1031f, .1030f, .0973f, .1099f), &null4);
    p4 += dot(p4, p4.wzxy + 33.33f);
    return fract((p4.xxyz + p4.yzzw) * p4.zywx, &null4);
}

//------------------------------------------------------------------------------
// 4 out, 4 in...
float4 hash44(float4 p4) {
    NULLS
	
    p4 = fract(p4 * (float4)(.1031f, .1030f, .0973f, .1099f), &null4);
    p4 += dot(p4, p4.wzxy + 33.33f);
    return fract((p4.xxyz + p4.yzzw) * p4.zywx, &null4);
}


#endif // __HASH_CL__