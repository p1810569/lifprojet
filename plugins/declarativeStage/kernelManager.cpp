// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QMap>
#include <QtGlobal>

#include "kernelManager.h"


KernelManager::Programs KernelManager::programs;
KernelManager::KernelMap KernelManager::kernels;


QCLKernel KernelManager::getKernel( const QCLProgram& program,
                                    const QString& name)
{
    cl_program id = program.programId();
    if (!id)
        return QCLKernel();

    if (Q_LIKELY(kernels.contains(id))) {
        Kernels& kers = kernels[id];

        if (kers.contains(name))
            return kers[name];

        QCLKernel kernel = program.createKernel(name);
        kers[name] = kernel;

        return kernel;
    }
    qWarning() << "Did not found program for kernel: " << name;

    return QCLKernel();
}


QCLProgram KernelManager::getProgram(QCLContext& ctx,
                                    const QString& path,
                                    const QString& options)
{
    if (Q_LIKELY(programs.contains(path)))
        return programs[path];

    QCLProgram program = ctx.buildProgramFromSourceFile(path, options);
    programs[path] = program;
    kernels[program.programId()] = {};

    return program;
}
