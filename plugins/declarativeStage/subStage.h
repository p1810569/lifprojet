// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef __SUBSTAGE_H__
#define __SUBSTAGE_H__

#include "subStageNode.h"


class SubStage;
class DeclarativeStage;


class KernelInput: public QObject {

    Q_OBJECT

public:
    KernelInput(int index);
    virtual ~KernelInput() = default;

    virtual void set(SubStage* substage) = 0;

    struct Info {
        QString id;
        int index;
    };
    const int index;
};



class KernelInputRessource: public KernelInput {

    Q_OBJECT

public:
    KernelInputRessource(SubStage& substage, Info& info);
    virtual ~KernelInputRessource() = default;

    virtual void set(SubStage* substage) override;

private:
    TerrainStage::DataPtr ressource;
};



class KernelInputInput: public KernelInput {

    Q_OBJECT

public:
    KernelInputInput(SubStage& substage, Info& info);
    virtual ~KernelInputInput() = default;

    virtual void set(SubStage* substage) override;

private:
    const QString id;
};


class KernelInputProperty: public KernelInput {

    Q_OBJECT

public:
    KernelInputProperty(SubStage& substage, Info& info);
    virtual ~KernelInputProperty() = default;

    virtual void set(SubStage* substage) override;

private:
    const QString id;
};


class KernelInputEnv: public KernelInput {

    Q_OBJECT

public:
    KernelInputEnv(SubStage& substage, Info& info);
    virtual ~KernelInputEnv() = default;

    virtual void set(SubStage* substage) override;

    enum Type {
        current_iteration,
        number_iteration,
        Invalalid = -1
    };
    Q_ENUMS(Type)

private:
    Type type;
};

/**
 * @brief       Constructeur de la classe SubStage
 * @param       data Terrain
 * @param       _stage Données sérialisées       
 */
class SubStage: public SubStageNode {

    Q_OBJECT

public:
    SubStage(const QJsonObject& data);
    ~SubStage();

    virtual void run(SubStageNode* parent) override;
    virtual void setup(DeclarativeStage* stage) override;

    typedef QSharedPointer<KernelInput> InputPtr;
    typedef InputPtr ProviderFunc(SubStage&, KernelInput::Info&);
    typedef QMap<QString, std::function<ProviderFunc>> Providers;

    static Providers providers;

    QCLKernel kernel;
    DeclarativeStage* stage;

protected:
    void addProvider(const QString& type, KernelInput::Info info);

    typedef QVector<QSharedPointer<KernelInput>> Inputs;
    Inputs inputs;
    
private:
};

#endif // __SUBSTAGE_H__