// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QJsonArray>

#include "subStageGroup.h"


void SubStageGroup::run(SubStageNode* parent) {

    SubStageNode::run(parent);

    for (NodeRef& child : children)
        child->exec(this);  
}


void SubStageGroup::setup(DeclarativeStage* stage) {

    SubStageNode::setup(stage);

    for (NodeRef& child : children)
        child->setup(stage);
}


SubStageGroup::SubStageGroup(const QJsonObject& data):
    SubStageNode(data)
{
    QJsonArray array = data["children"].toArray();
    children.reserve(array.size());

    for (const QJsonValueRef value : array) {
        NodeRef child = createNode(value.toObject());

        if (Q_LIKELY(child != Q_NULLPTR))
            children.append(qMove(child));
    }
}