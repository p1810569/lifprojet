// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "declarativeStage.h"
#include "kernelManager.h"
#include "subStage.h"


using DataPtr = DeclarativeStage::DataPtr;

SubStage::Providers SubStage::providers;


#define REGISTER_INPUT_TYPE(CLASS, ID) \
    INITIALIZER(provider) { \
        using Pointer = QSharedPointer<CLASS>; \
        SubStage::providers[ID] = [](SubStage& substage, KernelInput::Info& info) { \
            return Pointer::create(substage, info); \
        }; \
    }


INITIALIZER(substage_setting) {

    Application::onCreated([](Application*) {
        DeclarativeStage::addArg("-cl-kernel-arg-info");
    });
}


REGISTER_INPUT_TYPE(KernelInputEnv, "env")
REGISTER_INPUT_TYPE(KernelInputInput, "in")
REGISTER_INPUT_TYPE(KernelInputProperty, "prop")
REGISTER_INPUT_TYPE(KernelInputRessource, "res")



KernelInput::KernelInput(int _index):
    index(_index)
{

}


KernelInputRessource::KernelInputRessource(SubStage& substage, Info& info):
    KernelInput(info.index),
    ressource(substage.stage->getRessource(info.id))
{

    if (Q_UNLIKELY(ressource == Q_NULLPTR))
        ressource = RessourceFactory::getDefault("Null");
}

void KernelInputRessource::set(SubStage* substage) {

    ressource->set(substage->kernel, index);
}




KernelInputInput::KernelInputInput(SubStage& substage, Info& info):
    KernelInput(info.index),
    id(info.id)
{
    
}

void KernelInputInput::set(SubStage* substage) {

    DeclarativeStage* stage = substage->stage;
    DataPtr ressource = stage->getInput(id);
    
    if (Q_LIKELY(ressource != Q_NULLPTR))
        ressource->set(substage->kernel, index);
}




KernelInputProperty::KernelInputProperty(SubStage& substage, Info& info):
    KernelInput(info.index),
    id(info.id)
{

}


void KernelInputProperty::set(SubStage* substage) {

    #define CASE(ID, TYPE) case ID: \
        substage->kernel.setArg(index, variant.value<TYPE>()); break;

    DeclarativeStage* stage = substage->stage;
    QVariant variant = stage->terrain.property(id.toUtf8().data());

    switch (variant.type()) {
        CASE(QVariant::Double, float)
        CASE(QVariant::Int, cl_int)
        CASE(QVariant::UInt, cl_uint)
        CASE(QVariant::LongLong, cl_long)
        CASE(QVariant::Vector2D, QVector2D)
        CASE(QVariant::Vector3D, QVector3D)
        CASE(QVariant::Vector4D, QVector4D)
        CASE(QVariant::Point, QPoint)
        CASE(QVariant::PointF, QPointF)
        default:
            qWarning() << "Property" << id << " has invalid type.";
    }
}



KernelInputEnv::KernelInputEnv(SubStage& substage, Info& info):
    KernelInput(info.index)
{
    QMetaEnum enum_info = staticMetaObject.enumerator(0);

    int value = enum_info.keyToValue(info.id.toUtf8());
    type = static_cast<Type>(value);
}


void KernelInputEnv::set(SubStage* substage) {
    
    switch (type) {
        case current_iteration:
            substage->kernel.setArg(index, substage->current_iter()); break;
        case number_iteration:
            substage->kernel.setArg(index, substage->num_iter()); break;
        case Invalalid:
            qWarning() << "Input :" << substage->kernel.argName(index)
            << " for kernel :" << substage->kernel.name() << " is invalid.";
    }
}




SubStage::SubStage(const QJsonObject& data):
    SubStageNode(data),
    stage(Q_NULLPTR)
{

}


void SubStage::run(SubStageNode* parent) {

    SubStageNode::run(parent);

    if (Q_UNLIKELY(kernel.isNull()))
        return;

    for (QSharedPointer<KernelInput>& input: inputs)
        input->set(this);
    
    kernel.run();
}


void SubStage::setup(DeclarativeStage* _stage) {
    
    SubStageNode::setup(_stage);

    stage = _stage;
    QString kernel_id = data["kernel"].toString();

    if (kernel_id.isEmpty()) {
        qWarning() << "No kernel specified.";
        return;
    }
    // kernel = stage->program.createKernel(kernel_id);
    kernel = KernelManager::getKernel(stage->program, kernel_id);

    QJsonObject size_data = data["workSize"].toObject();

    QCLWorkSize best_size = kernel.bestLocalWorkSizeImage2D();
    QCLWorkSize group_size = kernel.declaredWorkGroupSize();

    if (group_size == QCLWorkSize(0,0))
        group_size = QCLWorkSize(
            size_t(size_data["width"].toInt(best_size.width())),
            size_t(size_data["height"].toInt(best_size.height()))
        );

    kernel.setGlobalWorkSize(stage->terrain.size());
    kernel.setLocalWorkSize(group_size);


    int num_args = kernel.argCount();
    inputs.clear();
    inputs.reserve(num_args);

    for (int i=0; i<num_args; ++i) {
        QString name = kernel.argName(i);
        QString type = name.section('_', 0, 0);

        if (providers.contains(type))
            addProvider(type, {name.section('_', 1, -1), i});
        else
            addProvider("res", {name, i});
    }
}


void SubStage::addProvider(const QString& type, KernelInput::Info info) {
    
    const auto create = providers.value(type);

    if (create) {
        InputPtr provider = create(*this, info);
        Q_ASSERT(provider != Q_NULLPTR);

        inputs.push_back(qMove(provider));
    }
}


