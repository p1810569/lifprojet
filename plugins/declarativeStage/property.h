// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef __PROPERTY_H__
#define __PROPERTY_H__

#include "ressourceData.h"
#include "utils.h"


class DeclarativeStage;


template<class T>
struct PropertyType { using type = Null; };


class PropertyInterface: public QObject {

    Q_OBJECT

public:
    virtual ~PropertyInterface() = default;

    QString label() const;
    QWidget* widget() const;

protected:
    PropertyInterface(  DeclarativeStage& stage,
                        QWidget* widget,
                        QString label);

    QWidget* prop_widget;
    QString label_text;
    DeclarativeStage& stage;
};




template<class T>
class Property: public PropertyInterface {

public:
    Property(DeclarativeStage& stage, const QJsonObject& data);
    virtual ~Property() = default;

    void changeValue();
    void updateValue();

    static void registerName(const QString& name);

    T* widget();

    typedef typename PropertyType<T>::type DataType;
    typedef T WidgetType;

private:
    void updated();

    typedef Ressource<DataType> ResPtr;
    std::shared_ptr<ResPtr> ressource;
};


#include "property.T"

#endif // __PROPERTY_H__