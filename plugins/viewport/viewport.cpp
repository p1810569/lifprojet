// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "application.h"
#include "viewport.h"

#include "shader-api.h"
#include <QOpenGLPixelTransferOptions>
#include <QOpenGLShader>
#include <QOpenGLTexture>


using namespace std;


TerrainViewWidget::TerrainViewWidget() {
	
	SetNearAndFarPlane(0.001, 1000.0);
	SetCamera(Camera(Vector(1.0, 1.0, 0.5), Vector::Null));

	Application* app = Application::globalInstance();

	connect(&app->terrain, SIGNAL(updateBuffer(Terrain::BufferType, QCLImage2D&)),
			this, SLOT(updateBuffer(Terrain::BufferType, QCLImage2D&)));

	connect(&app->terrain, SIGNAL(clearBuffer(Terrain::BufferType, Qt::GlobalColor)),
			this, SLOT(clearBuffer(Terrain::BufferType, Qt::GlobalColor)));

	connect(app->window.actionSave_Screenshot, &QAction::triggered, 
		[this](bool) { SaveScreen(); });

	if (!watcher.addPath("shaders/mesh.glsl"))
		qDebug() << "failed to watch: mesh.glsl";
	else
		connect(&watcher, SIGNAL(fileChanged(const QString&)),
				this, SLOT(reloadShader(const QString&)));
}


bool TerrainViewWidget::addShader(QOpenGLShader::ShaderTypeBit type,
									const QString& filename)
{
	return program.addShaderFromSourceFile(type, "shader:" + filename);
}


void TerrainViewWidget::setupShaders() {

	bool success = true;
	success &= addShader(QOpenGLShader::Vertex, "terrain.vert");
	success &= addShader(QOpenGLShader::Fragment, "terrain.frag");

    success &= program.link();
	success &= program.bind();

	Q_ASSERT(success);
}


void TerrainViewWidget::setupTextures() {

	QOpenGLTexture* texture = new QOpenGLTexture(QOpenGLTexture::Target2D);

    texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
    texture->setMagnificationFilter(QOpenGLTexture::LinearMipMapLinear);

    texture->setWrapMode(QOpenGLTexture::Repeat);
}


void TerrainViewWidget::initializeGL() {

	// GLenum err = glewInit();
	
	// if (err != GLEW_OK) {
	// 	qCritical() << "Error : " << glewGetErrorString(err);
	// 	std::cin.get();
	// 	exit(-1);
	// }

	// setupShaders();
	MeshWidget::initializeGL();


	Application* app = Application::globalInstance();

	for (const auto& it: app->objects.toStdMap())
		AddMesh(it.first, it.second);
	
	textures[Terrain::Height] = getTexture("terrain", "heightTex");
	textures[Terrain::Color] = getTexture("terrain", "colorTex");

	textures[Terrain::Height]->create();
	textures[Terrain::Color]->create();

	clearBuffer(Terrain::Height, Qt::black);
	clearBuffer(Terrain::Color, Qt::white);
}


void TerrainViewWidget::paintGL() {

	// glEnable(GL_DEPTH_TEST);
    // glEnable(GL_CULL_FACE);
	MeshWidget::paintGL();
}



QOpenGLTexture::PixelType getPixelType(QImage::Format fmt) {

	switch (fmt) {
	

        case QImage::Format_RGB444: 
        case QImage::Format_ARGB4444_Premultiplied: return QOpenGLTexture::UInt16_RGBA4;
	
		case QImage::Format_Mono:
        case QImage::Format_MonoLSB:
        case QImage::Format_Indexed8:
        case QImage::Format_Grayscale8:
        case QImage::Format_Alpha8:
        case QImage::Format_RGBX8888:
        case QImage::Format_RGBA8888:
        case QImage::Format_RGBA8888_Premultiplied:
        case QImage::Format_BGR888:
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
        case QImage::Format_ARGB32_Premultiplied:
        case QImage::Format_RGB888: return QOpenGLTexture::UInt8;

        case QImage::Format_Grayscale16: return QOpenGLTexture::Float16;
		
        case QImage::Format_RGBX64:
        case QImage::Format_RGBA64:
        case QImage::Format_RGBA64_Premultiplied: return QOpenGLTexture::UInt16;
	
        case QImage::Format_A2RGB30_Premultiplied:
        case QImage::Format_A2BGR30_Premultiplied:
        case QImage::Format_BGR30:
        case QImage::Format_RGB30: return QOpenGLTexture::UInt32_RGB10A2_Rev;
	
        case QImage::Format_RGB16: return QOpenGLTexture::UInt16_R5G6B5;

		default: break;
	}
	return QOpenGLTexture::NoPixelType;
}


QOpenGLTexture::PixelFormat getPixelFormat(QImage::Format fmt) {
	
	switch (fmt) {
        case QImage::Format_Alpha8: return QOpenGLTexture::Alpha;

        case QImage::Format_Grayscale8:
        case QImage::Format_Grayscale16:
        case QImage::Format_Indexed8:
        case QImage::Format_MonoLSB:
		case QImage::Format_Mono: return QOpenGLTexture::Red;

        case QImage::Format_RGB444:
        case QImage::Format_RGB555:
        case QImage::Format_RGB666:
        case QImage::Format_RGB888:
        case QImage::Format_RGB16:
        case QImage::Format_RGB30:
        case QImage::Format_RGB32: return QOpenGLTexture::RGB;

        case QImage::Format_RGBX8888:
        case QImage::Format_RGBA8888:
        case QImage::Format_RGBX64:
        case QImage::Format_RGBA64:
        case QImage::Format_RGBA64_Premultiplied:
        case QImage::Format_RGBA8888_Premultiplied: return QOpenGLTexture::RGBA;

        case QImage::Format_BGR888:
        case QImage::Format_BGR30: return QOpenGLTexture::BGR;

		case QImage::Format_ARGB32:
        case QImage::Format_ARGB32_Premultiplied: return QOpenGLTexture::RGBA;

		default: break;
	}
	return QOpenGLTexture::NoSourceFormat;
}




QOpenGLTexture::PixelType getPixelType(QCLImageFormat::ChannelType type) {

	switch (type) {
		
        case QCLImageFormat::Type_Unnormalized_Int8:
		case QCLImageFormat::Type_Normalized_Int8: return QOpenGLTexture::Int8;

        case QCLImageFormat::Type_Unnormalized_Int16:
        case QCLImageFormat::Type_Normalized_Int16: return QOpenGLTexture::Int16;

        case QCLImageFormat::Type_Unnormalized_UInt8:
        case QCLImageFormat::Type_Normalized_UInt8: return QOpenGLTexture::UInt8;

        case QCLImageFormat::Type_Unnormalized_UInt16:
        case QCLImageFormat::Type_Normalized_UInt16: return QOpenGLTexture::UInt16;

        case QCLImageFormat::Type_Normalized_565: return QOpenGLTexture::UInt16_R5G6B5;

        case QCLImageFormat::Type_Normalized_555: return QOpenGLTexture::UInt16_R5G6B5;

        case QCLImageFormat::Type_Normalized_101010: return QOpenGLTexture::UInt16_RGB5A1;

        case QCLImageFormat::Type_Unnormalized_UInt32:
        case QCLImageFormat::Type_Unnormalized_Int32: return QOpenGLTexture::Int32;

        case QCLImageFormat::Type_Half_Float: return QOpenGLTexture::Float16;
        case QCLImageFormat::Type_Float: return QOpenGLTexture::Float32;
	}

	return QOpenGLTexture::NoPixelType;
}


#define SELECT_CHANNEL_FORMAT(comp) \
	switch (format.channelType()) { \
		case QCLImageFormat::Type_Normalized_Int8: return QOpenGLTexture::comp##8_SNorm; \
		case QCLImageFormat::Type_Normalized_Int16: return QOpenGLTexture::comp##16_SNorm; \
		case QCLImageFormat::Type_Normalized_UInt8: return QOpenGLTexture::comp##8_UNorm; \
		case QCLImageFormat::Type_Normalized_UInt16: return QOpenGLTexture::comp##16_UNorm; \
		case QCLImageFormat::Type_Unnormalized_Int8: return QOpenGLTexture::comp##8I; \
		case QCLImageFormat::Type_Unnormalized_Int16: return QOpenGLTexture::comp##16I; \
		case QCLImageFormat::Type_Unnormalized_Int32: return QOpenGLTexture::comp##32I; \
		case QCLImageFormat::Type_Unnormalized_UInt8: return QOpenGLTexture::comp##8U; \
		case QCLImageFormat::Type_Unnormalized_UInt16: return QOpenGLTexture::comp##16U; \
		case QCLImageFormat::Type_Unnormalized_UInt32: return QOpenGLTexture::comp##32U; \
		case QCLImageFormat::Type_Half_Float: return QOpenGLTexture::comp##16F; \
		case QCLImageFormat::Type_Float: return QOpenGLTexture::comp##32F; \
		default: break; \
	} \
	break;


typedef std::array<QOpenGLTexture::SwizzleValue, 4> SwizzleMask;


QOpenGLTexture::TextureFormat getTextureFormat(QCLImageFormat format) {

	switch (format.channelOrder()) {
		case QCLImageFormat::Order_R: SELECT_CHANNEL_FORMAT(R)

		case QCLImageFormat::Order_Rx:
		case QCLImageFormat::Order_RG: SELECT_CHANNEL_FORMAT(RG)

		case QCLImageFormat::Order_RGx:
		case QCLImageFormat::Order_RGB: SELECT_CHANNEL_FORMAT(RGB)

		case QCLImageFormat::Order_ARGB:
		case QCLImageFormat::Order_BGRA:
		case QCLImageFormat::Order_RGBx:
		case QCLImageFormat::Order_RGBA: SELECT_CHANNEL_FORMAT(RGBA)
	}
	return QOpenGLTexture::NoFormat;
}



QOpenGLTexture::PixelFormat getPixelFormat(QCLImageFormat::ChannelOrder order) {

	switch (order) {
	
        case QCLImageFormat::Order_Intensity:
		case QCLImageFormat::Order_R: return QOpenGLTexture::Red;
        case QCLImageFormat::Order_A: return QOpenGLTexture::Alpha;

        case QCLImageFormat::Order_Rx:
        case QCLImageFormat::Order_RA:
        case QCLImageFormat::Order_RG: return QOpenGLTexture::RG;

        case QCLImageFormat::Order_RGx:
        case QCLImageFormat::Order_RGB: return QOpenGLTexture::RGB;
        case QCLImageFormat::Order_RGBA: return QOpenGLTexture::RGBA;

        case QCLImageFormat::Order_RGBx:
        case QCLImageFormat::Order_ARGB:
        case QCLImageFormat::Order_BGRA: return QOpenGLTexture::BGRA;

        case QCLImageFormat::Order_Luminence: return QOpenGLTexture::Luminance;
	}

	return QOpenGLTexture::NoSourceFormat;
}

SwizzleMask getTextureSwizzle(QCLImageFormat::ChannelOrder order) {

	using SV = QOpenGLTexture::SwizzleValue;

	switch (order) {
		case QCLImageFormat::Order_BGRA:
			return {SV::BlueValue, SV::GreenValue, SV::RedValue, SV::AlphaValue};
		case QCLImageFormat::Order_ARGB:
			return {SV::GreenValue, SV::BlueValue, SV::AlphaValue, SV::RedValue};
		default:
			break;
	}
	return {SV::RedValue, SV::GreenValue, SV::BlueValue, SV::AlphaValue};
}


void setupTexture(QOpenGLTexture* texture) {

	texture->setAutoMipMapGenerationEnabled(true);
	texture->setMaximumAnisotropy(16.0f);
	texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
	texture->setMagnificationFilter(QOpenGLTexture::LinearMipMapLinear);
	texture->setWrapMode(QOpenGLTexture::ClampToEdge);
	texture->generateMipMaps();
}


void TerrainViewWidget::clearBuffer(Terrain::BufferType type, Qt::GlobalColor color) {
	
	Application* app = Application::globalInstance();
	QOpenGLTexture* texture = textures[type];

	texture->destroy();

	QImage image(QSize(1,1), QImage::Format_RGBA8888);
	image.fill(color);

	texture->setData(image);

	setupTexture(texture);
}



void TerrainViewWidget::updateBuffer(Terrain::BufferType type, QCLImage2D& buffer) {
	
	Application* app = Application::globalInstance();
	QOpenGLTexture* texture = textures[type];

	texture->destroy();

	QCLImageFormat format = buffer.format();

	auto tex_format = getTextureFormat(format);
	auto swizzle = getTextureSwizzle(format.channelOrder());
	auto pixel_format = getPixelFormat(format.channelOrder());
	auto pixel_type = getPixelType(format.channelType());

	QVector<char> data(buffer.height() * buffer.bytesPerLine());
	QRect rect(0,0, buffer.width(), buffer.height());

	app->terrain.context.finish();

	// here we read to image data into the buffer
	if (!buffer.read(data.data(), rect, buffer.bytesPerLine()))
		return;


	texture->setFormat(tex_format);
	texture->setSwizzleMask(swizzle[0], swizzle[1], swizzle[2], swizzle[3]);
    texture->setSize(buffer.width(), buffer.height());
    texture->setMipLevels(texture->maximumMipLevels());
	texture->allocateStorage(pixel_format, pixel_type);

	QOpenGLPixelTransferOptions options;
	options.setAlignment(4);

	// then we transfere it back the the gpu texture storage
    texture->setData(0, pixel_format, pixel_type,
				(const char*)data.data(), &options);

	setupTexture(texture);
}


void TerrainViewWidget::reloadShader(const QString& filename) {
	
	if (reload_program(mainShaderProgram, filename.toUtf8()) != 0)
		program_print_errors(mainShaderProgram);
}




ViewportPlugin::ViewportPlugin():
	view_widget(new TerrainViewWidget)
{
    
}


QString ViewportPlugin::label() {
    return "Viewport";
}


QWidget* ViewportPlugin::widget() {
    return view_widget;
}
