// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __VIEWPORT_H__
#define __VIEWPORT_H__

#include "terrain.h"
#include "meshWidget.h"
#include "editorPlugin.h"

#include <QOpenGLShaderProgram>
#include <QFileSystemWatcher>



class TerrainViewWidget: public MeshWidget {

	Q_OBJECT

public:
	TerrainViewWidget();
	~TerrainViewWidget() = default;

protected:
    bool addShader(QOpenGLShader::ShaderTypeBit type, const QString& filename);

    void setupShaders();
    void setupTextures();

	virtual void initializeGL() override;
    virtual void paintGL() override;

    typedef std::array<QOpenGLTexture*, Terrain::COUNT> Textures;

    QOpenGLShaderProgram program;
    Textures textures;

    QFileSystemWatcher watcher;

private Q_SLOTS:
    void updateBuffer(Terrain::BufferType type, QCLImage2D& buffer);
    void clearBuffer(Terrain::BufferType type, Qt::GlobalColor color);

    void reloadShader(const QString& filename);
};

class ViewportPlugin: public EditorPlugin {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.alluvion.editor")
    Q_INTERFACES(EditorPlugin)

public:
    ViewportPlugin();
    ~ViewportPlugin() = default;

    virtual QString label() override;
    virtual QWidget* widget() override;

    TerrainViewWidget* view_widget;
};

#endif // __VIEWPORT_H__