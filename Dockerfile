FROM debian:latest
RUN apt-get update && apt-get install -y \
    g++ \
    qtcreator \
    qttools5-dev \
    qtdeclarative5-dev \
    qtbase5-private-dev \
    cmake \
    libprotobuf-dev \
    protobuf-compiler \
    git \
    ocl-icd-libopencl1 \
    ocl-icd-opencl-dev \
    opencl-headers \
    clinfo \
    libglew-dev \
    freeglut3
